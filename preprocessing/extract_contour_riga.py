from PIL import Image
import numpy as np
import os
import utils

dirnames = ["../../data/segmentation/riga/BinRushed1-Corrected", "../../data/segmentation/riga/BinRushed2",
            "../../data/segmentation/riga/BinRushed3", "../../data/segmentation/riga/BinRushed4",
            "../../data/segmentation/riga/MESSIDOR", "../../data/segmentation/riga/MagrabiFemale",
            "../../data/segmentation/riga/MagrabiaMale"]

for dirname in dirnames:
    fnames = utils.all_files_under(dirname, contain_string="prime")
    for num, fname in enumerate(fnames):
        for i in range(1, 7):
            mask_fname = fname.replace("prime", "-{}".format(i))
            if not os.path.exists(mask_fname):
                mask_fname = fname.replace("prime", "-{}".format(i)).replace(".jpg", ".tif")
            img = np.array(Image.open(mask_fname))
            h, w, _ = img.shape
            contours = np.zeros((h, w))
            contours[(img[..., 0] == 46) & (img[..., 1] > 20) & (img[..., 1] > 80)] = 255
            contours[(img[..., 0] > 20) & (img[..., 0] < 100) & (img[..., 1] > 30) & (img[..., 1] < 80) & (img[..., 2] < 170)] = 255
            contours[(img[..., 0] > 90) & (img[..., 0] < 120) & (img[..., 1] > 50) & (img[..., 1] < 90) & (img[..., 2] > 30) & (img[..., 2] < 50)] = 255
            
            dir_contour_checks = os.path.dirname(fname) + "_contours"
            utils.mkdir_if_not_exist(dir_contour_checks)
            contour_check_path = os.path.join(dir_contour_checks, os.path.basename(mask_fname))
            Image.fromarray(contours.astype(np.uint8)).save(contour_check_path)
