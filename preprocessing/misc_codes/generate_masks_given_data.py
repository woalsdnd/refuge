# generate mask for riga dataset
# python generate_masks
# assume that the folders for RIGA dataset are under the same folder

from PIL import Image
import numpy as np
import os
from scipy.ndimage import binary_fill_holes
from skimage import measure


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


value_disc = 128
value_cup = 0
dirnames = ["Glaucoma", "Non-Glaucoma"]

for dirname in dirnames:
    fnames = all_files_under(dirname)
    out_dir = dirname + "_mask"
    dir_disc = os.path.join(out_dir, "disc")
    dir_cup = os.path.join(out_dir, "cup")
    mkdir_if_not_exist(dir_disc)
    mkdir_if_not_exist(dir_cup)
    for fname in fnames:
        gt = np.array(Image.open(fname))
        fbasename = os.path.basename(fname)
        
        # save disc mask
        disc_mask = np.zeros(gt.shape)
        disc_mask[gt <= value_disc] = 255
        Image.fromarray(disc_mask.astype(np.uint8)).save(os.path.join(dir_disc, fbasename))
        
        # save cup mask
        disc_mask = np.zeros(gt.shape)
        disc_mask[gt == value_cup] = 255
        Image.fromarray(disc_mask.astype(np.uint8)).save(os.path.join(dir_cup, fbasename))
        