import argparse
import numpy as np
import os
from PIL import Image
from skimage import measure
from scipy.ndimage import binary_dilation, binary_erosion, generate_binary_structure, binary_fill_holes
import utils


def contour2seg_mask(contour_path, out_dir):
    contour = np.array(Image.open(contour_path))
    contour_fname = os.path.basename(contour_path)
    dir_disc = os.path.join(out_dir, "disc")
    dir_cup = os.path.join(out_dir, "cup")
    utils.make_new_dir(dir_disc)
    utils.make_new_dir(dir_cup)
    
    # Find Connected Components with intensity above the threshold
    val_contour = 255
    blobs_labels, n_blobs = measure.label(contour == val_contour, return_num=True)
    if n_blobs == 0:
        return
    
    # sort by number
    list_n_pixels = []
    for i in range(1, n_blobs):
        list_n_pixels.append((i, len(contour[blobs_labels == i])))
    sorted_n_pixels = sorted(list_n_pixels, key=lambda x: x[1], reverse=True)
    
    # save disc mask
    mask_disc = np.zeros(contour.shape)
    mask_disc[blobs_labels == sorted_n_pixels[0][0]] = 1
    mask_disc = binary_dilation(mask_disc, generate_binary_structure(2, 2), iterations=10).astype(np.uint8)
    mask_disc = binary_fill_holes(mask_disc)
    mask_disc = binary_erosion(mask_disc, generate_binary_structure(2, 2), iterations=10).astype(np.uint8)
    mask_disc *= 255
    Image.fromarray(contour.astype(np.uint8)).save(os.path.join(dir_disc, contour_fname))
    
    # save cup mask
    mask_cup = np.zeros(contour.shape)
    mask_cup[blobs_labels == sorted_n_pixels[1][0]] = 1
    mask_cup = binary_dilation(mask_cup, generate_binary_structure(2, 2), iterations=10).astype(np.uint8)
    mask_cup = binary_fill_holes(mask_cup)
    mask_cup = binary_erosion(mask_cup, generate_binary_structure(2, 2), iterations=10).astype(np.uint8)
    mask_cup *= 255
    Image.fromarray(mask_cup.astype(np.uint8)).save(os.path.join(dir_cup, contour_fname))


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--contour_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

utils.make_new_dir(FLAGS.out_dir)
contour_paths = utils.all_files_under(FLAGS.contour_dir)
        
for contour_path in contour_paths:
    contour2seg_mask(contour_path, FLAGS.out_dir)
