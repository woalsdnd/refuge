# check if mask has more than 500 pixels for foreground in riga dataset
# python check_mask
# assume that the folders for RIGA dataset are under the same folder

from PIL import Image
import numpy as np
import os
from skimage import measure


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


val_contour = 255
dirnames = ["BinRushed1-Corrected_mask", "BinRushed2_mask", "BinRushed3_mask", "BinRushed4_mask", "MESSIDOR_mask", "MagrabiFemale_mask", "MagrabiaMale_mask"]

for dirname in dirnames:
    types = all_files_under(dirname)
    for type in types:
        fnames = all_files_under(type)
        for fname in fnames:
            mask_img = np.array(Image.open(fname))
            val_contour = 255
            
            # Find Connected Components with intensity above the threshold
            blobs_labels, n_blobs = measure.label(mask_img == val_contour, return_num=True)
            if n_blobs == 0:
                print "{} failed".format(contour_path)
                continue
            
            # sort by number
            list_n_pixels = []
            for i in range(1, n_blobs + 1):
                list_n_pixels.append((i, len(blobs_labels[blobs_labels == i])))
            sorted_n_pixels = sorted(list_n_pixels, key=lambda x: x[1], reverse=True)
            
            if len(blobs_labels[blobs_labels == sorted_n_pixels[0][0]]) < 500:
                print fname
