import argparse
import numpy as np
import os
from PIL import Image
import utils

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--od_seg_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--contour_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

od_filenames = utils.all_files_under(FLAGS.od_seg_dir)
utils.make_new_dir(FLAGS.out_dir)
        
for od_fname in od_filenames:
    od_seg = np.array(Image.open(od_fname))
    fname = os.path.basename(od_fname)
    for i in range(1, 7):
        contour_fname = os.path.join(FLAGS.contour_dir, fname.replace("prime", "-{}".format(i)))
        if not os.path.exists(contour_fname):
            contour_fname = os.path.join(FLAGS.contour_dir, fname.replace("prime", "-{}".format(i)).replace(".jpg", ".tif"))
        contour = np.array(Image.open(contour_fname))
    
        # save overlap of od and the contours
        overlap = np.zeros(od_seg.shape)
        overlap[(od_seg == 255) & (contour == 255)] = 255
        Image.fromarray((overlap).astype(np.uint8)).save(os.path.join(FLAGS.out_dir, os.path.basename(contour_fname)))
        
