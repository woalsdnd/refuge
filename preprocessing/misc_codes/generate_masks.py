# generate mask for riga dataset
# python generate_masks
# assume that the folders for RIGA dataset are under the same folder

from PIL import Image
import numpy as np
import os
from scipy.ndimage import binary_fill_holes
from skimage import measure


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


val_contour = 255
dirnames = ["BinRushed1-Corrected", "BinRushed2", "BinRushed3", "BinRushed4", "MESSIDOR", "MagrabiFemale", "MagrabiaMale"]

for dirname in dirnames:
    fnames = all_files_under(dirname, contain_string="prime")
    out_dir = dirname + "_mask"
    dir_disc = os.path.join(out_dir, "disc")
    dir_cup = os.path.join(out_dir, "cup")
    mkdir_if_not_exist(dir_disc)
    mkdir_if_not_exist(dir_cup)
    for num, fname in enumerate(fnames):
        fundus_img = np.array(Image.open(fname).convert('L')).astype(np.float32)
        for i in range(1, 7):
            contour_path = fname.replace("prime", "-{}".format(i))
            if not os.path.exists(contour_path):
                contour_path = fname.replace("prime", "-{}".format(i)).replace(".jpg", ".tif")
            contour_img = np.array(Image.open(contour_path).convert('L')).astype(np.float32)
            h, w = contour_img.shape
            diff_img = abs(fundus_img - contour_img)
            diff_img[diff_img > 10] = val_contour
            diff_img[diff_img <= 10] = 0
                
            # Find Connected Components with intensity above the threshold
            blobs_labels, n_blobs = measure.label(diff_img == val_contour, return_num=True)
            if n_blobs == 0:
                print "{} failed".format(contour_path)
                continue
            
            # sort by number
            list_n_pixels = []
            for i in range(1, n_blobs + 1):
                list_n_pixels.append((i, len(diff_img[blobs_labels == i])))
            sorted_n_pixels = sorted(list_n_pixels, key=lambda x: x[1], reverse=True)
            
            # save disc mask
            mask_disc = np.zeros(diff_img.shape)
            mask_disc[blobs_labels == sorted_n_pixels[0][0]] = 1
            mask_disc = binary_fill_holes(mask_disc).astype(np.uint8)
            mask_disc *= 255
            contour_fname = os.path.basename(contour_path)
            Image.fromarray(mask_disc.astype(np.uint8)).save(os.path.join(dir_disc, contour_fname))
            
            # save cup mask
            mask_cup = np.zeros(diff_img.shape)
            mask_cup[blobs_labels == sorted_n_pixels[1][0]] = 1
            mask_cup = binary_fill_holes(mask_cup).astype(np.uint8)
            mask_cup *= 255
            Image.fromarray(mask_cup.astype(np.uint8)).save(os.path.join(dir_cup, contour_fname))
