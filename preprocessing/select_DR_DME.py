import numpy as np
import os
from subprocess import Popen, PIPE
import pandas as pd


def get_gla_candi(df, label_filepath):
    label_filename = os.path.basename(label_filepath)
    if label_filename == "IDRiD.csv":
        return df[(df["Retinopathy grade"] != 0) | (df["Risk of macular edema "] != 0)]["Image name"].map(lambda x:x + ".png")
    elif label_filename == "kaggle_test.csv":
        return df[df["level"] != 0]["image"].map(lambda x:x + ".png")
    elif label_filename == "kaggle_train.csv":
        return df[df["level"] != 0]["image"].map(lambda x:x + ".png")
    elif label_filename == "messidor.csv":
        return df[(df["Retinopathy grade"] != 0) | (df["Risk of macular edema "] != 0)]["Image name"].map(lambda x:x.replace(".tif", ".png"))
          

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


# set misc paths
label_home_dir = "../../data/labels"
label_paths = [os.path.join(label_home_dir, fn) for fn in ["IDRiD.csv", "kaggle_test.csv", "kaggle_train.csv", "messidor.csv"]]
preprocessed_home_dir = "../../data/preprocessed"
out_dir = "../../data/preprocessed/glaucoma_candidate_v0"
mkdir_if_not_exist(out_dir)

for label_path in label_paths:
    df = pd.read_csv(label_path)
    gla_candi_filenames = get_gla_candi(df, label_path)
    for gla_candi_filename in gla_candi_filenames:
        intermediate_dirname = os.path.basename(label_path).replace(".csv", "")
        from_path = os.path.join(preprocessed_home_dir, intermediate_dirname, gla_candi_filename)
        to_path = os.path.join(out_dir, gla_candi_filename)
        pipes = Popen(["cp", from_path, to_path], stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()        
