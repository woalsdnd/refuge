import utils
import numpy as np
import os
from subprocess import Popen, PIPE

riga_home = "/home/woalsdnd/data/segmentation/riga"
types = ["cup", "disc"]
for type in types:
    path = os.path.join(riga_home, type)
    img_dir_path = os.path.join(path, "imgs")
    mask_dir_path = os.path.join(path, "masks")
    img_paths = utils.all_files_under(img_dir_path)
    mask_paths = utils.all_files_under(mask_dir_path)
    
    # set dictionary
    img_dict, mask_dict, ext_dict = {}, {}, {}
    for img_path in img_paths:
        key = os.path.basename(img_path).split("prime")[0]
        img_dict[key] = 0
    for mask_path in mask_paths:
        key = os.path.basename(mask_path).replace(".jpg", "").replace(".tif", "")
        mask_dict[key] = 0
    for mask_path in mask_paths:
        key = os.path.basename(mask_path).replace(".jpg", "").replace(".tif", "")
        ext_dict[key] = os.path.basename(mask_path).split(".")[1] 

    # mark imgs to remain
    for key, val in img_dict.iteritems():
        if key + "-1" in mask_dict and key + "-2" in mask_dict:
            img_dict[key] = 1
            mask_dict[key + "-1"] = 1
            mask_dict[key + "-2"] = 1

    # rm no matches
    for key, val in img_dict.iteritems():
        if val == 0:
            pipes = Popen("rm {}".format(os.path.join(img_dir_path, key + "*")), shell=True, stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()
    for key, val in mask_dict.iteritems():
        if val == 0:
            pipes = Popen("rm {}".format(os.path.join(mask_dir_path, key + "*")), shell=True, stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()

    # cp files according to mask files
    for key, val in img_dict.iteritems():
        pipes = Popen("cp {} {}".format(os.path.join(img_dir_path, key + "prime*"), os.path.join(img_dir_path, key + "-1." + ext_dict[key + "-1"])), shell=True, stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate() 
        pipes = Popen("cp {} {}".format(os.path.join(img_dir_path, key + "prime*"), os.path.join(img_dir_path, key + "-2." + ext_dict[key + "-2"])), shell=True, stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate() 
        pipes = Popen("rm {}".format(os.path.join(img_dir_path, key + "prime*")), shell=True, stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate() 
           
