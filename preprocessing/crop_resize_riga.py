
from PIL import Image

import numpy as np
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize


def replace_img_format(file_basename, extension):
    file_basename = file_basename.replace("." + extension, ".png")
    file_basename = file_basename.replace("." + extension, ".png")
    return file_basename

    
def process(args):
    h_target, w_target, img_filepaths, mask_filepaths, out_dir_home = args
    for index in range(len(img_filepaths)):
        img_filename = img_filepaths[index]
        mask_filename = mask_filepaths[index]
        assert os.path.basename(img_filename) == os.path.basename(mask_filename)
        
        # skip processed files
        out_img_dir = os.path.join(out_dir_home, "images")
        out_mask_dir = os.path.join(out_dir_home, "masks")
        out_img_path = os.path.join(out_img_dir, os.path.basename(img_filename).replace(".tif", "").replace(".jpg", "") + ".png")
        out_mask_path = os.path.join(out_mask_dir, os.path.basename(img_filename).replace(".tif", "").replace(".jpg", "") + ".png")
        if os.path.exists(out_img_path) and os.path.exists(out_mask_path):
            continue
        
        # read the image and resize 
        img = np.array(Image.open(img_filename))
        mask = np.array(Image.open(mask_filename))
        h_ori, w_ori, _ = np.shape(img)
        red_threshold = 20
        roi_check_len = h_ori // 5
        
        # Find Connected Components with intensity above the threshold
        blobs_labels, n_blobs = measure.label(img[:, :, 0] > red_threshold, return_num=True)
        if n_blobs == 0:
            print ("crop failed for %s " % (img_filename)
                    + "[no blob found]")
            continue
       
        # Find the Index of Connected Components of the Fundus Area (the central area)
        majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                                w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
        if majority_vote == 0:
            print ("crop failed for %s " % (img_filename)
                    + "[invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")
            continue
        
        row_inds, col_inds = np.where(blobs_labels == majority_vote)
        row_max = np.max(row_inds)
        row_min = np.min(row_inds)
        col_max = np.max(col_inds)
        col_min = np.min(col_inds)
        if row_max - row_min < 100 or col_max - col_min < 100:
            print n_blobs
            for i in range(1, n_blobs + 1):
                print len(blobs_labels[blobs_labels == i])
            print ("crop failed for %s " % (img_filename)
                    + "[too small areas]")
            continue
        
        # crop the image 
        crop_img = img[row_min:row_max, col_min:col_max]
        crop_mask = mask[row_min:row_max, col_min:col_max]
        max_len = max(crop_img.shape[0], crop_img.shape[1])
        img_h, img_w, _ = crop_img.shape
        padded_img = np.zeros((max_len, max_len, 3))
        padded_img[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
        resized_img = imresize(padded_img, (h_target, w_target), 'bilinear')
        
        padded_mask = np.zeros((max_len, max_len))
        padded_mask[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w] = crop_mask
        resized_mask = imresize(padded_mask, (h_target, w_target), 'bilinear')
        
        # save cropped image
        Image.fromarray(resized_img.astype(np.uint8)).save(out_img_path)
        Image.fromarray(resized_mask.astype(np.uint8)).save(out_mask_path)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


h_target, w_target = 640, 640
n_processes = 20

riga_home = "/home/woalsdnd/data/segmentation/riga"
types = ["cup", "disc"]
for type in types:
    path = os.path.join(riga_home, type)
    img_dir_path = os.path.join(path, "imgs")
    mask_dir_path = os.path.join(path, "masks")

    # make out_dir if not exists
    preprocessed_home_dir = "/home/woalsdnd/data/preprocessed/"
    out_dir_home = os.path.join(preprocessed_home_dir, "cup_segmentation") if type == "cup" else os.path.join(preprocessed_home_dir, "od_segmentation")
    out_dir_home = os.path.join(out_dir_home, "riga")
    out_img_dir = os.path.join(out_dir_home, "images")
    out_mask_dir = os.path.join(out_dir_home, "masks")
    if not os.path.isdir(out_img_dir):
        os.makedirs(out_img_dir)
    if not os.path.isdir(out_mask_dir):
        os.makedirs(out_mask_dir)

    # divide files
    img_files = all_files_under(img_dir_path)
    mask_files = all_files_under(mask_dir_path)
    img_filenames = [[] for __ in xrange(n_processes)]
    mask_filenames = [[] for __ in xrange(n_processes)]
    chunk_sizes = len(img_files) // n_processes
    for index in xrange(n_processes):
        if index == n_processes - 1:  # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(img_files)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        img_filenames[index] = img_files[start:end]
        mask_filenames[index] = mask_files[start:end]
    
    # run multiple processes
    pool = multiprocessing.Pool(processes=n_processes)
    pool.map(process, [(h_target, w_target, img_filenames[i], mask_filenames[i], out_dir_home) for i in range(n_processes)])
