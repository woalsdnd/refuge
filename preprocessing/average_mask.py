import utils
import os
import numpy as np
from PIL import Image
from scipy.ndimage.morphology import binary_fill_holes
from skimage import measure

imgfnames = utils.all_files_under("../../data/segmentation/rimone/disc/imgs/")
dir_masks_all = "../../data/segmentation/rimone/disc/masks_all/"
dir_masks = "../../data/segmentation/rimone/disc/masks/"

for imgfname in imgfnames:
    imgfname = os.path.basename(imgfname)
    masks = []
    for i in range(1, 6):
        masks.append(utils.imagefiles2arrs([os.path.join(dir_masks_all, imgfname.replace(".bmp", "") + "-exp{}.bmp".format(i))]))
    masks = np.concatenate(masks, axis=0)
    
    # get average
    avr_mask = np.mean(masks, axis=0)
    avr_mask_rounded = np.round(avr_mask)
    
    # find the center blob
    roi_check_len = 10
    h_ori, w_ori = avr_mask_rounded.shape
    blobs_labels, n_blobs = measure.label(avr_mask_rounded == 1, return_num=True)
    if n_blobs == 0:
        print ("no blob detected for {}".format(imgfname))
        continue
    majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                            w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
    if majority_vote == 0:
        print ("background more dominant than the center blob for {}".format(imgfname))
        continue
    avr_mask_rounded_center_blob = np.zeros(avr_mask_rounded.shape)
    avr_mask_rounded_center_blob[blobs_labels == majority_vote] = 1
    
    # fill in holes
    avr_mask_rounded_filled = binary_fill_holes(avr_mask_rounded_center_blob).astype(np.uint8)
    
    Image.fromarray((avr_mask_rounded_filled).astype(np.uint8)).save(os.path.join(dir_masks, imgfname.replace(".bmp", "") + ".png"))
    
