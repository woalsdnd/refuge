import argparse
import os
import utils
import time
import pandas as pd
import numpy as np

out_dir = "/home/woalsdnd/data/labels/"


def localization_inference(img_dir):
    # set paths
    vessel_model_dir = "model/vessel"
    localization_model_dir = "model/od_fovea_localization"
    filepaths = utils.all_files_under(img_dir)
    out_csv_path = os.path.join(out_dir, os.path.basename(img_dir) + "_pts.csv")

    # load the models and corresponding weights
    vessel_model = utils.load_network(vessel_model_dir)
    localization_model = utils.load_network(localization_model_dir)
    
    # iterate all images
    start_time = time.time()
    filenames, list_y_disc, list_x_disc, list_y_fovea, list_x_fovea = [], [], [], [], []
    for filepath in filepaths:
        # load an img (tensor shape of [1,h,w,3])
        img = utils.imagefiles2arrs([filepath])
        _, h, w, d = img.shape
        assert h == 512 and w == 512
        resized_img = utils.resize_img(img, 640, 640)
    
        # run inference
        vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
        predicted_coords, _ = localization_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"), (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
        
        filenames.append(os.path.basename(filepath))
        list_y_disc.append(predicted_coords[0, 0])
        list_x_disc.append(predicted_coords[0, 1])
        list_y_fovea.append(predicted_coords[0, 2])
        list_x_fovea.append(predicted_coords[0, 3])

    df_pts = pd.DataFrame({'filename': filenames, 'x_disc':list_x_disc, 'y_disc':list_y_disc,
                            'x_fovea':list_x_fovea, 'y_fovea':list_y_fovea})
    df_pts.to_csv(out_csv_path, index=False)
    
    print "duration: {} sec".format(time.time() - start_time)


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# training settings 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# localization
localization_inference(FLAGS.img_dir)
