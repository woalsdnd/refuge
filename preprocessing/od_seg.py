import os
import utils
import numpy as np
import pandas as pd
import time
from PIL import Image
import argparse


def segmentation_inference(img_dir, out_dir):
    # set paths
    vessel_model_dir = "model/vessel"
    segmentation_model_dir = "model/od_from_fundus_vessel"

    # set params
    process_len = 640
        
    # load the models and corresponding weights
    vessel_model = utils.load_network(vessel_model_dir)
    od_from_fundus_vessel_model = utils.load_network(segmentation_model_dir)
    
    # make directories
    utils.make_new_dir(out_dir)
        
    # iterate all images
    filenames = utils.all_files_under(img_dir)
    for filename in filenames:
        # load an img (tensor shape of [1,h,w,3])
        img = utils.imagefiles2arrs([filename])
        _, h, w, _ = img.shape
        resized_img, y_offset, x_offset, cropped_w = utils.crop_resize_ocular_fundus(img, process_len, process_len)

        # run inference
        vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
        od_seg, _ = od_from_fundus_vessel_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"), (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
        
        # cut by threshold
        od_seg = od_seg[0, ..., 0]
        od_seg_zoomed_to_ori_scale = utils.sizeup(od_seg, y_offset, x_offset, cropped_w, h, w)
        od_seg_zoomed_to_ori_scale = utils.cut_by_threshold(od_seg_zoomed_to_ori_scale, threshold=0.2)
        od_seg_zoomed_to_ori_scale_out = od_seg_zoomed_to_ori_scale * 255
        
        # save the result
        Image.fromarray((od_seg_zoomed_to_ori_scale_out).astype(np.uint8)).save(os.path.join(out_dir, os.path.basename(filename)))


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--fundus_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# training settings 
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)

# segmentation
segmentation_inference(FLAGS.fundus_dir, FLAGS.out_dir)

