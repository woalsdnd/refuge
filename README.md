# REGURE Challenge  #

## File Hierarchy ##
gla_candidate: DR & DME  
no_gla_candidate: no DR & no DME

## Running the Code ##
``` python crop_resize.py --img_dir=original/messidor/ --out_dir=preprocessed/messidor --n_processes=20 --w_target=512 --h_target=512 --extension=tif ``` 

``` python select_no_DR_no_DME.py ``` 

```python train.py --batch_size=24 --gpu_index=0 --lambda_val=1 --load_model_dir=models_1.0/ > out_exp  ```

```python filter_candidate.py```

```python localize_od_fovea.py --img_dir=../../data/preprocessed/Training400/Glaucoma --gpu_index=0 ```

```python inference.py --gpu_index=0 --img_dir=../../data/preprocessed/glaucoma_candidate_v0/ --out_path=../../outputs/classification_output/candidate_gla_v0_preliminary_model.csv --img_anal_dir=../../outputs/img_analysis/candidate_gla_v0_preliminary_model --model_dir=../../models/preliminary_model/ > out_exp ```

```python select_DR_DME.py ```

```python select_no_DR_no_DME.py```

```python extract_vessels.py --img_dir=../../data/preprocessed/localization/validation/ --out_dir=../../outputs/vessels_localization/validation/ --gpu_index=3```

```python train_fovea_localization.py --batch_size=5 --gpu_index=3 --limit_vessel_loss=True```

```python resize_mask.py --img_dir=../../data/segmentation/Training400/disc/masks/ --out_dir=../../data/preprocessed/segmentation/validation/masks/ --n_processes=32 --w_target=640 --h_target=640 --extension=bmp```

```python train.py --gpu_index=5 --batch_size=1```


## LICENSE ##
This is under the MIT License  
Copyright (c) 2018 Vuno Inc. (www.vuno.co)  