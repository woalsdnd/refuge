import pandas as pd
import utils
import os


def labels(val_ratio, add_external_data=False):
    # load df for pts (coordinate range [0,1])
    pts_files = utils.all_files_under("../../data/labels", contain_string="pts")
    list_df = []
    for pts_file in pts_files:
        list_df.append(pd.read_csv(pts_file))
    df_pts = pd.concat(list_df, ignore_index=True)
    
    # make df for classification
    glaucoma_paths = utils.all_files_under("../../data/preprocessed/Training400/Glaucoma", append_path=False)
    non_glaucoma_paths = utils.all_files_under("../../data/preprocessed/Training400/Non-Glaucoma", append_path=False)
    df_gla_cls = pd.DataFrame({'filename': glaucoma_paths, 'label':[1] * len(glaucoma_paths)})
    df_no_gla_cls = pd.DataFrame({'filename': non_glaucoma_paths, 'label':[0] * len(non_glaucoma_paths)})
    if add_external_data:
        glaucoma_paths_external = utils.all_files_under("../../data/preprocessed/filtered_manually/gla_candidates", append_path=False)
        #glaucoma_paths_external = utils.all_files_under("../../data/preprocessed/filtered_manually/gla_candidates_1st_attempt", append_path=False)
        non_glaucoma_paths_external1 = utils.all_files_under("../../data/preprocessed/filtered_by_algorithm/no_gla_in_gla_candidate", append_path=False)
        non_glaucoma_paths_external2 = utils.all_files_under("../../data/preprocessed/filtered_by_algorithm/no_gla_in_no_gla_candidate", append_path=False)
        df_gla_cls_external = pd.DataFrame({'filename': glaucoma_paths_external, 'label':[1] * len(glaucoma_paths_external)})
        df_no_gla_cls_external1 = pd.DataFrame({'filename': non_glaucoma_paths_external1, 'label':[0] * len(non_glaucoma_paths_external1)})
        df_no_gla_cls_external2 = pd.DataFrame({'filename': non_glaucoma_paths_external2, 'label':[0] * len(non_glaucoma_paths_external2)})
        
    # merge pts info with cls info
    df_gla_given_dataset = pd.merge(df_gla_cls, df_pts, on="filename", how="left")
    df_no_gla_given_dataset = pd.merge(df_no_gla_cls, df_pts, on="filename", how="left")
    if add_external_data:
        df_gla_external = pd.merge(df_gla_cls_external, df_pts, on="filename", how="left")
        df_no_gla_external1 = pd.merge(df_no_gla_cls_external1, df_pts, on="filename", how="left")
        df_no_gla_external2 = pd.merge(df_no_gla_cls_external2, df_pts, on="filename", how="left")
    df_gla_given_dataset["filename"] = df_gla_given_dataset["filename"].map(lambda x:os.path.join("../../data/preprocessed/Training400/Glaucoma", x))
    df_no_gla_given_dataset["filename"] = df_no_gla_given_dataset["filename"].map(lambda x:os.path.join("../../data/preprocessed/Training400/Non-Glaucoma", x))
    if add_external_data:
        df_gla_external["filename"] = df_gla_external["filename"].map(lambda x:os.path.join("../../data/preprocessed/filtered_manually/gla_candidates", x))
        df_no_gla_external1["filename"] = df_no_gla_external1["filename"].map(lambda x:os.path.join("../../data/preprocessed/filtered_by_algorithm/no_gla_in_gla_candidate", x))
        df_no_gla_external2["filename"] = df_no_gla_external2["filename"].map(lambda x:os.path.join("../../data/preprocessed/filtered_by_algorithm/no_gla_in_no_gla_candidate", x))
    if add_external_data:
        df_merged = pd.concat([df_gla_given_dataset, df_no_gla_given_dataset, df_gla_external, df_no_gla_external1, df_no_gla_external2], ignore_index=True)
    else:
        df_merged = pd.concat([df_gla_given_dataset, df_no_gla_given_dataset], ignore_index=True)
        
    # set region
    df_merged["region"] = df_merged.label.map(lambda x:"IT | ST | SD | ID" if x == 1 else "")
    # split into data and test datasets
    df_gla = df_merged[df_merged.label == 1]
    df_no_gla = df_merged[df_merged.label == 0]
    df_val_gla = df_gla.iloc[:int(len(df_gla) * val_ratio)]
    df_train_gla = df_gla.iloc[int(len(df_gla) * val_ratio):]
    df_val_no_gla = df_no_gla.iloc[:int(len(df_no_gla) * val_ratio)]
    df_train_no_gla = df_no_gla.iloc[int(len(df_no_gla) * val_ratio):]

    df_val = pd.concat([df_val_gla, df_val_no_gla], ignore_index=True)
    df_train = pd.concat([df_train_gla, df_train_no_gla], ignore_index=True)
    
    return df_train, df_val
