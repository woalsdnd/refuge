import numpy as np
import pandas as pd
import argparse
from sklearn.metrics import roc_auc_score

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_csv',
    type=str,
    required=False
    )
FLAGS, _ = parser.parse_known_args()

pred_fovea = pd.read_csv(FLAGS.pred_csv)
pred_fovea["gts"] = pred_fovea["id"].map(lambda x:1 if x[0] == 'g' else 0)

ids = np.array(pred_fovea["id"])
gts = np.array(pred_fovea["gts"])
preds = np.array(pred_fovea["pred"])

print "false positive: {}".format(ids[(gts == 0) & (preds >= 0.5)])
print "false negative: {}".format(ids[(gts == 1) & (preds < 0.5)])
auroc_score = roc_auc_score(gts, preds)
print "AUROC SCORE : {}".format(auroc_score)