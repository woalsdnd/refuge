import pandas as pd
from subprocess import Popen, PIPE
import utils
import os

# set misc paths
# gla_check_out_dir = "../../outputs/img_analysis/gla_in_candidate_no_gla_v0_preliminary_model"
# gla_dir = "../../data/preprocessed/glaucoma_from_external" 
# no_gla_dir = "../../data/preprocessed/no_glaucoma_from_external" 
gla_check_out_dir = "../../outputs/img_analysis/gla_in_candidate_gla_v0_preliminary_model"
gla_dir = "../../data/preprocessed/filtered_by_algorithm/gla_in_gla_candidate" 
no_gla_dir = "../../data/preprocessed/filtered_by_algorithm/no_gla_in_gla_candidate" 
utils.mkdir_if_not_exist(gla_check_out_dir)
utils.mkdir_if_not_exist(gla_dir)
utils.mkdir_if_not_exist(no_gla_dir)
# csv_file = "../../outputs/classification_output/candidate_no_gla_v0_preliminary_model.csv"
csv_file = "../../outputs/classification_output/candidate_gla_v0_preliminary_model.csv"

# set thresholds
no_gla_threshold = 0.1
gla_threshold = 0.5

# print #imgs by threshold
df = pd.read_csv(csv_file)
print "# of no-gla & gla images by threshold"
print "| thr | no_gla | gla |"
for i in range(10):
    print "| {} | {} | {} |".format(i * 0.1, len(df[df.pred <= i * 0.1]), len(df[df.pred > i * 0.1]))
    print "-----------------------"

# cp imgs by predicted value
df_gla = df[df.pred > gla_threshold]
df_no_gla = df[df.pred < no_gla_threshold]
for gla_fn in df_gla.filename:
    pipes = Popen(["cp", gla_fn, gla_dir], stdout=PIPE, stderr=PIPE)
    std_out, std_err = pipes.communicate()
for no_gla_fn in df_no_gla.filename:
    pipes = Popen(["cp", no_gla_fn, no_gla_dir], stdout=PIPE, stderr=PIPE)
    std_out, std_err = pipes.communicate()

# check glaucoma suspect imgs
# activation_dir = "../../outputs/img_analysis/candidate_no_gla_v0_preliminary_model"
# for gla_fn in df_gla.filename:
#     pipes = Popen(["cp", gla_fn, gla_check_out_dir], stdout=PIPE, stderr=PIPE)
#     std_out, std_err = pipes.communicate()
#     pipes = Popen(["cp", os.path.join(activation_dir, os.path.basename(gla_fn)), gla_check_out_dir], stdout=PIPE, stderr=PIPE)
#     std_out, std_err = pipes.communicate()
