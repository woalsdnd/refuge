import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import models
import label_provider_given_data_validation
import numpy as np
from keras.models import model_from_json

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=int,
    required=True
    )
parser.add_argument(
    '--lambda_val',
    type=float,
    required=True
    )
parser.add_argument(
    '--load_model_dir',
    type=str,
    required=False,
    default=None
    )
FLAGS, _ = parser.parse_known_args()

# set misc params
n_epochs = 100
n_epochs = 300
# val_ratio = 0.1
# schedules = {'lr':{'0': 0.001, '80':0.0001}}
schedules = {'lr':{'0': 0.001, '100':0.0001, '200':0.00001}}
model_dir = "models_{}".format(FLAGS.lambda_val)
lambda_val = FLAGS.lambda_val
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)
if not os.path.isdir(model_dir):
    os.makedirs(model_dir)

# training settings
train_filenames_labels, val_filenames_labels = label_provider_given_data_validation.labels()
weight = utils.class_weight(train_filenames_labels)
#train_batch_fetcher = iterator.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, "normalize")
#validation_batch_fetcher = iterator.ValidationBatchFetcher(val_filenames_labels, FLAGS.batch_size, "normalize")
train_batch_fetcher = iterator.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, "instance_std")
validation_batch_fetcher = iterator.ValidationBatchFetcher(val_filenames_labels, FLAGS.batch_size, "instance_std")

# create & save network
img_shape = (512, 512, 3)
if FLAGS.load_model_dir:
    network_file = utils.all_files_under(FLAGS.load_model_dir, extension=".json")
    weight_file = utils.all_files_under(FLAGS.load_model_dir, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network = models.set_optimizer(network, lambda_val)
    print "model loaded from files"
else:
    network = models.network_activation_out(img_shape, lambda_val)
    print "model from the scratch"
network.summary()
with open(os.path.join(model_dir, "network.json"), 'w') as f:
    f.write(network.to_json())

# train the network 
scheduler = utils.Scheduler(schedules, schedules['lr']['0'])
check_train_batch, check_test_batch = True, True
auroc_best = 0
training_losses = {"activation":[0] * n_epochs, "class":[0] * n_epochs}
val_losses = {"activation":[0] * n_epochs, "class":[0] * n_epochs, "total":[0] * n_epochs, "acc_class":[0] * n_epochs}
for epoch in range(n_epochs):
    # update step sizes, learning rates, lambda
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())    
    
    # train on train set
    training_loss = {"loss_activation":[], "loss_class":[], "acc_class":[]}
    start_time = time.time()
    for filenames, batch_x, batch_y, batch_lm, batch_mask in train_batch_fetcher():
        if check_train_batch:
            utils.plot_imgs((batch_x * 255), "../../outputs/input_checks/{}/train/imgs".format(model_dir))
            utils.plot_imgs((batch_mask * 255), "../outputs/input_checks/{}/train/masks".format(model_dir))
            check_train_batch = False
        loss_total, loss_class, loss_activation, acc_class, acc_activation = network.train_on_batch(batch_x, [batch_y, np.expand_dims(batch_mask[:, ::32, ::32], axis=3)])
        training_loss["loss_activation"] += [loss_activation] * len(filenames)
        training_loss["loss_class"] += [loss_class] * len(filenames)
        training_loss["acc_class"] += [acc_class] * len(filenames)
    training_losses["activation"][epoch] = np.mean(training_loss["loss_activation"])
    training_losses["class"][epoch] = np.mean(training_loss["loss_class"])
    training_acc = np.mean(training_loss["acc_class"])
    print "activation loss: {}".format(training_losses["activation"][epoch])
    print "classification loss: {}".format(training_losses["class"][epoch])
    print "classification acc: {}".format(training_acc)
    
    # evaluate on val set
    pred_labels = []
    true_labels = []
    for filenames, batch_x, batch_y, batch_lms, batch_mask in validation_batch_fetcher():
        if check_test_batch:
            utils.plot_imgs(batch_x, "../../outputs/input_checks/{}/val/imgs".format(model_dir))
            utils.plot_imgs((batch_mask * 255), "../../outputs/input_checks/{}/val/masks".format(model_dir))
            check_test_batch = False
        preds, activation = network.predict(batch_x)
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
    utils.print_stats(true_labels, pred_labels)
    HM_curr, auroc_curr = utils.get_metric(["HM", "auroc"], true_labels, pred_labels)
    duration = time.time() - start_time
    print "{}th epoch ==> duration : {}".format(epoch, duration)
    
    # save weights
    network.save_weights(os.path.join(model_dir, "weight_{}epoch.h5".format(epoch)))
    if auroc_curr > auroc_best:
        auroc_best = auroc_curr
        HM_at_best_auroc = HM_curr
        network.save_weights(os.path.join(model_dir, "weight_best_auroc.h5"))
    sys.stdout.flush()
print "best auroc : {}, HM at best auroc : {}".format(auroc_best, HM_at_best_auroc)
