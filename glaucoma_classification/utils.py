import os
import re

from PIL import Image, ImageEnhance

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
import random
from skimage.transform import warp, AffineTransform
from subprocess import Popen, PIPE
import math
import mask_region
from scipy.misc import imresize
from skimage import color
from keras import backend as K
from keras.models import model_from_json
from PIL import ImageFilter
import matplotlib.cm as cm


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def class_weight(df):
    data_ratio = df["label"].value_counts(normalize=True)
    weight = np.array([1. / data_ratio[0], 1. / data_ratio[1]])
    weight /= sum(weight)
    print "data number"
    print df["label"].value_counts(normalize=False)
    print "class weight : {}".format(weight)
    return weight


def analyze_imgs(filenames, true_labels, pred_labels, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    pred_labels = outputs2labels(pred_labels, 0, 1)

    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            
            std_out, std_err = pipes.communicate()


def analyze_imgs_inference(filenames, activations, out_dir):
    mkdir_if_not_exist(out_dir)
    h_target, w_target = 512, 512
    for i in range(len(filenames)):
        img = np.array(Image.open(filenames[i])).astype(np.float32)
        activation = activations[i, :, :, 0]
        heatmap = (activation - np.min(activation)) / (np.max(activation) - np.min(activation))
        heatmap = imresize(heatmap, (h_target, w_target), 'bilinear')
        heatmap_pil_img = Image.fromarray(heatmap)
        heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(32))
        heatmap_blurred = np.asarray(heatmap_pil_img)
        heatmap_blurred = np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
        overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
        Image.fromarray(overlapped).save(os.path.join(out_dir, os.path.basename(filenames[i]).replace(".png", "") + "_superimposed.png"))


def get_mask_of_regions(img_size, disc_center, macula_center, regions):
    region_codes = ["None", "M", "T", "ST", "IT", "SN", "IN", "SD", "ID"]
    mask = np.zeros(img_size)
    for index in range(1, len(regions)):
        if regions[index] == 1:
            mask[(mask == 1) | (mask_region.mask(img_size, disc_center, macula_center, region_codes[index]) == 1)] = 1
    return mask


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def codify_regions(list_region):
    # input : list of strings of regions
    # output : numpy matrix of codified regions
    n_regions = 8
    region_code = {"":0, "M":1, "T":2, "ST":3, "IT":4, "SN":5, "IN":6, "SD":7, "ID":8}

    codified_regions = np.zeros((len(list_region), n_regions + 1))
    for index, region_str in enumerate(list_region):
        for region in [chunk.strip() for chunk in region_str.split("|")]:
            codified_regions[index, region_code[region]] = 1
    return codified_regions


def rotate_pt(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    """
    rad = angle * math.pi / 180
    oy, ox = origin
    py, px = point
    qx = ox + math.cos(rad) * (px - ox) - math.sin(rad) * (py - oy)
    qy = oy + math.sin(rad) * (px - ox) + math.cos(rad) * (py - oy)
    return qy, qx


def dist(pt1, pt2):
    return np.sqrt((pt2[0] - pt1[0]) ** 2 + (pt2[1] - pt1[1]) ** 2)


def out_circle(pt, c_pt, r):
    return dist(pt, c_pt) > r


def random_perturbation(img):
    rescale_factor = 1.2
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def load_augmented(fname, lms, regions, normalize, augment):
    # read image file
    ori_img, img_aug, lms_aug = load_imgs_lms([fname], [lms], normalize, augment)
    ori_img = ori_img[0, ...]
    img_aug = img_aug[0, ...]
    lms_aug = lms_aug[0, ...]
    region_mask = get_mask_of_regions((img_aug.shape[:2]), lms_aug[:2], lms_aug[2:], regions)
    region_mask[ori_img[..., 0] < 10] = 0
    assert len(img_aug.shape) == 3 and lms_aug.shape == (4,) and len(region_mask.shape) == 2
    return img_aug, lms_aug, region_mask


def load_imgs_lms(filenames, lms, normalize=None, augment=True):
    # filenames, lms : list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    lm_pts = np.zeros((len(filenames), 4), dtype=np.float32)
    for index, lm in enumerate(lms):
        lm_pts[index, :] = lm * img_shape[0]
    
    # convert to z score per image
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        # remove low values & erase time 
        img[img < 10] = 0
        h, w, d = img.shape

        if augment:
            # random color, contrast, brightness perturbation
            img = random_perturbation(img)
            
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
                lm_pts[file_index, 1] = w - lm_pts[file_index, 1]  # x_disc
                lm_pts[file_index, 3] = w - lm_pts[file_index, 3]  # x_fovea
           
            # affine transform (scale, rotation)
            shift_y, shift_x = np.array(img.shape[:2]) / 2.
            shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
            shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
            r_angle = random.randint(0, 359)
            scale_factor = 1.15
            scale = random.uniform(1. / scale_factor, scale_factor)
            tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
            img = warp(img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img_color = np.copy(img)

            # rotate landmarks
            lm_pts[file_index, 0] = scale * (lm_pts[file_index, 0] - shift_y)
            lm_pts[file_index, 1] = scale * (lm_pts[file_index, 1] - shift_x)
            lm_pts[file_index, 2] = scale * (lm_pts[file_index, 2] - shift_y)
            lm_pts[file_index, 3] = scale * (lm_pts[file_index, 3] - shift_x)
            lm_pts[file_index, :2] = rotate_pt((0, 0), (lm_pts[file_index, 0], lm_pts[file_index, 1]), r_angle)
            lm_pts[file_index, 2:] = rotate_pt((0, 0), (lm_pts[file_index, 2], lm_pts[file_index, 3]), r_angle)
            lm_pts[file_index, 0] += shift_y
            lm_pts[file_index, 1] += shift_x
            lm_pts[file_index, 2] += shift_y
            lm_pts[file_index, 3] += shift_x
            
            # tailored augmentation
            if normalize == "data":
                # perturb colors
                for i in range(3):
                    img += np.random.normal(0, 1) * sqrt_eigenvalues[i] * eigenvectors[i, ...]
            elif normalize == "instance_mean":
                img = img * (1 + random.random() * 0.1) if random.getrandbits(1) else img * (1 - random.random() * 0.1)
                img = np.clip(img, 0, 255)
        else:
            img_color = np.copy(img)
      
        ori_images_arr[file_index] = img_color

        if normalize == "instance_std":
            means = np.zeros(3)
            stds = np.array([255.0, 255.0, 255.0])
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
                    std_val = np.std(img_color[..., i][img_color[..., i] > 10])
                    stds[i] = std_val if std_val > 0 else 255
            images_arr[file_index] = (img - means) / stds
        elif normalize == "data_std":
            means = data_means
            stds = data_stds
            images_arr[file_index] = (img - means) / stds
        elif normalize == "instance_mean":
            means = np.zeros(3)
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
            images_arr[file_index] = img - means
        elif normalize == "normalize":
            images_arr[file_index] = 1.*img / 255.0
        elif normalize == "Lab": 
            img_lab = color.rgb2lab(img.astype(np.uint8))
            if augment:
                intensity_scale_factor, color_scale_factor = 1.2, 1.2
                L_scale = random.uniform(1. / intensity_scale_factor, intensity_scale_factor)
                a_scale = random.uniform(1. / color_scale_factor, color_scale_factor)
                b_scale = random.uniform(1. / color_scale_factor, color_scale_factor)
                img_lab[..., 0] *= L_scale
                img_lab[..., 1] *= a_scale
                img_lab[..., 2] *= b_scale
            img_lab[..., 0] = (img_lab[..., 0] - 50) / 50.
            img_lab[..., 1:] /= 128.
            images_arr[file_index] = img_lab
        else:
            images_arr[file_index] = img.astype(np.float32)
        
    return ori_images_arr, images_arr, lm_pts


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def print_stats(y_true, y_pred):
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    HM = 2 * spe * sen / (spe + sen)
    AUC_ROC = roc_auc_score(y_true, y_pred)
    
    print cm
    print "specificity : {},  sensitivity : {}, harmonic mean : {},  ROC_AUC : {} ".format(spe, sen, HM, AUC_ROC)   


def get_metric(metrics, y_true, y_pred):
    values = []
    for metric in metrics:
        if metric == "HM":
            cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
            spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
            sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
            HM = 2 * spe * sen / (spe + sen)
            values.append(HM)
        elif metric == "auroc":
            AUC_ROC = roc_auc_score(y_true, y_pred)
            values.append(AUC_ROC)

    return values


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        images_arr[file_index] = np.array(Image.open(filenames[file_index]))
    
    return images_arr


def normalize(imgs):
    images_arr = 1.*imgs / 255.0
    return images_arr

def z_score(imgs):
    images_arr = np.zeros(imgs.shape)
    for index in range(imgs.shape[0]):
        img = imgs[index,...]
        img[img < 10] = 0
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                stds[i] = std_val if std_val > 0 else 255
        images_arr[index] = (img - means) / stds
    return images_arr

def resize_img(img, target_size):
    if len(img.shape) == 4:  # fundus img
        n, h, w, d = img.shape
        resized_img = np.zeros((n, target_size[0], target_size[1], d))
        for index in range(n):
            resized_img[index, ...] = imresize(img[index, ...], target_size, 'bilinear')
        return resized_img
    else:  # result mask
        assert len(img.shape) == 2
        h_from, w_from = img.shape
        h_to, w_to = target_size
        return zoom(img, (1.*h_to / h_from, 1.*w_to / w_from), order=2)


def plot_imgs(imgs, out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for i in range(imgs.shape[0]):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(os.path.join(out_dir, "imgs_{}.png".format(i + 1)))
        Image.fromarray(imgs[i, ::32, ::32].astype(np.uint8)).save(os.path.join(out_dir, "imgs_discrete_{}.png".format(i + 1)))
        resized_img = imresize(imgs[i, ...], (16, 16), 'bilinear')
        Image.fromarray(resized_img.astype(np.uint8)).save(os.path.join(out_dir, "imgs_resized_{}.png".format(i + 1)))

        
def merge_files(path):
    file_lists = os.listdir(path)
    total_img_name = []
    for folder_name in file_lists:
        total_img_name.extend(all_files_under(os.path.join(path, folder_name)))
    return total_img_name


class Scheduler:

    def __init__(self, schedules, init_lr):
        self.schedules = schedules
        self.lr = init_lr

    def get_lr(self):
        return self.lr
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr']:
            self.lr = self.schedules['lr'][key]
