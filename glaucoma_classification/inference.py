import utils
import os
import argparse
import time
import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
#     default="/home/woalsdnd/data/preprocessed/Training400",
#     default="/home/woalsdnd/data/Online_validation",
    default="/home/woalsdnd/data/Offline_validation",
    required=False
    )
parser.add_argument(
    '--out_path',
    type=str,
#     default="/home/woalsdnd/outputs/glaucoma_classification/Online_validation",
    default="/home/woalsdnd/outputs/glaucoma_classification/Offline_validation",
#     default="/home/woalsdnd/outputs/glaucoma_classification/training",
    required=False
    )
parser.add_argument(
    '--img_anal_dir',
    type=str,
#     default="/home/woalsdnd/outputs/glaucoma_classification/Oline_validation/gla_cls_img_anal",
    default="/home/woalsdnd/outputs/glaucoma_classification/Offline_validation/gla_cls_img_anal",
    required=False
    )
parser.add_argument(
    '--model_dir',
    type=str,
    default="/home/woalsdnd/models/glaucoma_classification",
    required=False
    )
parser.add_argument(
    '--validation_purpose',
    type=bool,
    default=False,
    required=False
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load a network
network = utils.load_network(FLAGS.model_dir)

# run inference
start_time = time.time()
utils.mkdir_if_not_exist(FLAGS.img_anal_dir)

# merge image in two directories
if FLAGS.validation_purpose:
    filenames = utils.merge_files(FLAGS.img_dir)
else:
    filenames = utils.all_files_under(FLAGS.img_dir)

# run inference
gts, preds = [], []
fns = []
target_size = (512, 512)
for filename in filenames:
    fn = os.path.basename(filename)
    fns.append(fn)
    print "processing {}...".format(fn)
        
    img = utils.imagefiles2arrs([filename])
    resized_img = utils.resize_img(img, target_size)
#     normalized_resized_img = utils.normalize(resized_img)
    normalized_resized_img = utils.z_score(resized_img)
    pred, activation = network.predict(normalized_resized_img)
    preds.append(pred[0, 0])
#     utils.analyze_imgs_inference([filename], activation, FLAGS.img_anal_dir)

    if FLAGS.validation_purpose:
        gt = 0 if "n" in filename.split("/")[-1].split(".")[0] else 1
        gts.append(gt)
    
# save to csv
df = pd.DataFrame({"FileName":fns, "Glaucoma Risk":preds})
utils.mkdir_if_not_exist(FLAGS.out_path)
#print df[df["Glaucoma Risk"]>0.5]
df.to_csv(os.path.join(FLAGS.out_path, "submit.csv"), index=False)

if FLAGS.validation_purpose:
    gts, preds, fns = np.array(gts), np.array(preds), np.array(fns)
    print "false positive: {}".format(fns[(gts == 0) & (preds >= 0.5)])
    print "false negative: {}".format(fns[(gts == 1) & (preds < 0.5)])
    auroc_score = roc_auc_score(gts, preds)
    print "AUROC SCORE : {}".format(auroc_score)
    duration = time.time() - start_time
    print "duration : {}".format(duration)
