import pandas as pd
import utils
import os


def labels():
    n_gla_given_data = 40
    n_no_gla_given_data = 360
    
    # load df for pts (coordinate range [0,1])
    pts_files = utils.all_files_under("../../data/labels", contain_string="pts")
    list_df = []
    for pts_file in pts_files:
        list_df.append(pd.read_csv(pts_file))
    df_pts = pd.concat(list_df, ignore_index=True)
    
    # make df for classification
    glaucoma_paths = utils.all_files_under("../../data/preprocessed/Training400/Glaucoma", append_path=False)
    non_glaucoma_paths = utils.all_files_under("../../data/preprocessed/Training400/Non-Glaucoma", append_path=False)
    df_gla_cls = pd.DataFrame({'filename': glaucoma_paths, 'label':[1] * len(glaucoma_paths)})
    df_no_gla_cls = pd.DataFrame({'filename': non_glaucoma_paths, 'label':[0] * len(non_glaucoma_paths)})
    glaucoma_external_dir="../../data/preprocessed/filtered_by_algorithm/gla_candidates"
    non_glaucoma_external_dir="../../data/preprocessed/filtered_by_algorithm/no_gla_candidates"
    glaucoma_paths_external = utils.all_files_under(glaucoma_external_dir, append_path=False)
    non_glaucoma_paths_external = utils.all_files_under(non_glaucoma_external_dir, append_path=False)
    df_gla_cls_external = pd.DataFrame({'filename': glaucoma_paths_external, 'label':[1] * len(glaucoma_paths_external)})
    df_no_gla_cls_external = pd.DataFrame({'filename': non_glaucoma_paths_external, 'label':[0] * len(non_glaucoma_paths_external)})
            
    # merge pts info with cls info
    df_gla_given_dataset = pd.merge(df_gla_cls, df_pts, on="filename", how="left")
    df_no_gla_given_dataset = pd.merge(df_no_gla_cls, df_pts, on="filename", how="left")
    df_gla_external = pd.merge(df_gla_cls_external, df_pts, on="filename", how="left")
    df_no_gla_external = pd.merge(df_no_gla_cls_external, df_pts, on="filename", how="left")
    df_gla_given_dataset["filename"] = df_gla_given_dataset["filename"].map(lambda x:os.path.join("../../data/preprocessed/Training400/Glaucoma", x))
    df_no_gla_given_dataset["filename"] = df_no_gla_given_dataset["filename"].map(lambda x:os.path.join("../../data/preprocessed/Training400/Non-Glaucoma", x))
    df_gla_external["filename"] = df_gla_external["filename"].map(lambda x:os.path.join(glaucoma_external_dir, x))
    df_no_gla_external["filename"] = df_no_gla_external["filename"].map(lambda x:os.path.join(non_glaucoma_external_dir, x))
    df_merged = pd.concat([df_gla_given_dataset, df_no_gla_given_dataset, df_gla_external, df_no_gla_external], ignore_index=True)
        
    # set region
    df_merged["region"] = df_merged.label.map(lambda x:"IT | ST | SD | ID" if x == 1 else "")
    # split into data and test datasets
    df_gla = df_merged[df_merged.label == 1]
    df_no_gla = df_merged[df_merged.label == 0]
    df_val_gla = df_gla.iloc[:n_gla_given_data]
    df_train_gla = df_gla.iloc[n_gla_given_data:]
    df_val_no_gla = df_no_gla.iloc[:n_no_gla_given_data]
    df_train_no_gla = df_no_gla.iloc[n_no_gla_given_data:]

    df_val = pd.concat([df_val_gla, df_val_no_gla], ignore_index=True)
    df_train = pd.concat([df_train_gla, df_train_no_gla], ignore_index=True)
    
    return df_train, df_val
