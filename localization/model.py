import os

from keras import backend as K
from keras import objectives
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dense
from keras.layers import Input
from keras.layers.core import Activation, Flatten
from keras.layers.merge import Concatenate
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers.core import Lambda
import numpy as np
from keras.optimizers import SGD

os.environ['KERAS_BACKEND'] = 'tensorflow'
K.set_image_dim_ordering('tf')


def conv_blocks(n_block, x, n_filters, filter_size, padding='same', strides=(1, 1), dilation_rate=(1, 1)):
    conv = x
    for _ in range(n_block):
        conv = Conv2D(n_filters, filter_size, strides=strides, padding=padding, dilation_rate=dilation_rate)(conv)
        conv = BatchNormalization(scale=False, axis=3)(conv)
        conv = Activation('relu')(conv) 
    return conv


def weights_for_activation2coord(act_h, act_w):
    l_h = 1. / act_h
    l_w = 1. / act_w
    weights = np.zeros((act_h * act_w, 2))
    for i in range(act_h * act_w):
        weights[i, 0] = (0.5 + i / act_w) * l_h
        weights[i, 1] = (0.5 + i % act_h) * l_w
    return np.expand_dims(weights, axis=0)


def localize_fv(img_size, limit_vessel_loss=False, lambda_val=0.3):
    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    img_ch_vessel = 1  # image channels
    out_ch_vessel = 1  # output channel
    img_ch_fundus = 3  # image channels
    img_height, img_width = img_size[0], img_size[1]
    n_filters_vessel = 16
    n_filters_fundus = 32
    padding = 'same'
    
    input_vessel = Input((img_height, img_width, img_ch_vessel))
    list_out = [input_vessel]
    
    list_out_fovea = [list_out[-1]]
    for index in range(5):
        conv_fovea = conv_blocks(2, list_out_fovea[-1], 2 ** (index) * n_filters_fundus, (k, k), padding=padding)
        pool_fovea = MaxPooling2D(pool_size=(s, s))(conv_fovea)
        list_out_fovea.append(pool_fovea)
    
    list_dilated_conv = []
    list_dilated_conv.append(Conv2D(8 * n_filters_vessel, (k, k), padding=padding)(list_out_fovea[-1]))
    list_dilated_conv.append(Conv2D(8 * n_filters_vessel, (k, k), dilation_rate=(2, 2), padding=padding)(list_out_fovea[-1]))
    list_dilated_conv.append(Conv2D(8 * n_filters_vessel, (k, k), dilation_rate=(4, 4), padding=padding)(list_out_fovea[-1]))
    vessel_block_fovea = Concatenate(axis=3)(list_dilated_conv)  

    fovea_activation_vessel = Conv2D(out_ch_vessel, (1, 1), padding=padding, activation='sigmoid')(vessel_block_fovea)
    
    flattened_fovea_vessel = Flatten()(fovea_activation_vessel)
    
    normalized_flattened_fovea_vessel = Lambda(lambda x:x / K.sum(x, axis=1, keepdims=True))(flattened_fovea_vessel)
    
    img2coord_vessel = Dense(2, activation=None, use_bias=False, name="img2coord_vessel")
    img2coord_vessel.trainable = False
    coord_fovea_vessel = img2coord_vessel(normalized_flattened_fovea_vessel)
    
    input_fundus = Input((img_height, img_width, img_ch_fundus))
    list_out = [input_fundus]
    list_out_fovea = [list_out[-1]]
    for index in range(4):
        conv_fovea = conv_blocks(2, list_out_fovea[-1], 2 ** (index) * n_filters_fundus, (k, k), padding=padding)
        pool_fovea = MaxPooling2D(pool_size=(s, s))(conv_fovea)
        list_out_fovea.append(pool_fovea)
        
    fovea_concat_block = Concatenate(axis=3)([list_out_fovea[-1], UpSampling2D(size=(s, s))(vessel_block_fovea)])
    conv_fovea_final = conv_blocks(2, fovea_concat_block, 16 * n_filters_fundus, (k, k), padding=padding)
    activation_fovea = Conv2D(1, (1, 1), padding=padding, activation='sigmoid')(conv_fovea_final)
    
    flattened_fovea = Flatten()(activation_fovea)
    
    normalized_flattened_fovea = Lambda(lambda x:x / K.sum(x, axis=1, keepdims=True))(flattened_fovea)

    img2coord = Dense(2, activation=None, use_bias=False, name="img2coord")
    img2coord.trainable = False
    coord_fovea = img2coord(normalized_flattened_fovea)

    model = Model([input_fundus, input_vessel], [coord_fovea, coord_fovea_vessel])
    model.get_layer("img2coord").set_weights(weights_for_activation2coord(activation_fovea._keras_shape[1], activation_fovea._keras_shape[2]))
    model.get_layer("img2coord_vessel").set_weights(weights_for_activation2coord(fovea_activation_vessel._keras_shape[1], fovea_activation_vessel._keras_shape[2]))

    def loss_vessel(y_true, y_pred):
        diff = K.abs(y_true - y_pred)
        clipped_diff = K.tf.where(K.tf.less(diff, 0.015), 0 * diff, diff)
        return K.mean(clipped_diff, axis=-1)
    
    if limit_vessel_loss:
       model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=["mean_absolute_error", loss_vessel], loss_weights=[1, lambda_val], metrics=None)
    else:
        model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=["mean_absolute_error", "mean_absolute_error"], loss_weights=[1, lambda_val], metrics=None)

    return model


def localize_fv_no_vessel(img_size):
    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    img_ch_vessel = 1  # image channels
    out_ch_vessel = 1  # output channel
    img_ch_fundus = 3  # image channels
    img_height, img_width = img_size[0], img_size[1]
    n_filters_vessel = 16
    n_filters_fundus = 32
    padding = 'same'
    
    input_fundus = Input((img_height, img_width, img_ch_fundus))
    list_out = [input_fundus]
    list_out_fovea = [list_out[-1]]
    for index in range(4):
        conv_fovea = conv_blocks(2, list_out_fovea[-1], 2 ** (index) * n_filters_fundus, (k, k), padding=padding)
        pool_fovea = MaxPooling2D(pool_size=(s, s))(conv_fovea)
        list_out_fovea.append(pool_fovea)
        
    conv_fovea_final = conv_blocks(2, list_out_fovea[-1], 16 * n_filters_fundus, (k, k), padding=padding)
    activation_fovea = Conv2D(1, (1, 1), padding=padding, activation='sigmoid')(conv_fovea_final)
    
    flattened_fovea = Flatten()(activation_fovea)
    
    normalized_flattened_fovea = Lambda(lambda x:x / K.sum(x, axis=1, keepdims=True))(flattened_fovea)

    img2coord = Dense(2, activation=None, use_bias=False, name="img2coord")
    img2coord.trainable = False
    coord_fovea = img2coord(normalized_flattened_fovea)

    model = Model(input_fundus, coord_fovea)
    model.get_layer("img2coord").set_weights(weights_for_activation2coord(activation_fovea._keras_shape[1], activation_fovea._keras_shape[2]))
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss="mean_absolute_error", metrics=None)

    return model
