import numpy as np
from model import localize_fv
import utils
import os
import argparse
from keras import backend as K
import iterator_localization
import sys

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    help="gpu index",
    required=True
    )
parser.add_argument(
    '--batch_size',
    type=int,
    help="batch size",
    required=True
    )
parser.add_argument(
    '--limit_vessel_loss',
    type=bool,
    required=True
    )
parser.add_argument(
    '--lambda_val',
    type=float,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# training settings 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
n_epochs = 1000
validation_epochs = range(0, 500, 50) + range(500, 800, 10) + range(800, 1000, 1)
batch_size = FLAGS.batch_size
schedules = {'lr':{'0': 0.001, '500':0.0001}}

# set misc paths
img_size = (640, 640)
feature_map_size = (20, 20)
model_out_dir = "models_fovea_limit_vessel_loss_{}".format(FLAGS.lambda_val) if FLAGS.limit_vessel_loss else "models_fovea_{}".format(FLAGS.lambda_val) 
vessel_train_dir = "../../outputs/vessels_localization/training"
vessel_val_dir = "../../outputs/vessels_localization/validation"
fundus_train_dir = "../../data/preprocessed/localization/training"
fundus_val_dir = "../../data/preprocessed/localization/validation"
img_out_dir = "../../outputs/localized_fovea_limit_vessel_loss_{}".format(FLAGS.lambda_val)  if FLAGS.limit_vessel_loss else "../../outputs/localized_fovea_{}".format(FLAGS.lambda_val) 
fovea_label_path_training = "../../data/localization/IDRiD_Fovea_Center_Training_set.csv"
fovea_label_path_validation = "../../data/localization/Fovea_location.csv"
if not os.path.isdir(model_out_dir):
    os.makedirs(model_out_dir)
if not os.path.isdir(img_out_dir):
    os.makedirs(img_out_dir)
 
# set iterators for training and validation
train_batch_fetcher = iterator_localization.TrainBatchFetcher(fundus_train_dir, vessel_train_dir, fovea_label_path_training, FLAGS.batch_size)
validation_batch_fetcher = iterator_localization.ValidationBatchFetcher(fundus_val_dir, vessel_val_dir, fovea_label_path_validation, FLAGS.batch_size)

# create networks
network = localize_fv(img_size, FLAGS.limit_vessel_loss, FLAGS.lambda_val) 
network.summary()
with open(os.path.join(model_out_dir, "network.json"), 'w') as f:
    f.write(network.to_json())

# start training
scheduler = utils.Scheduler(schedules)
Best_Euclidean = 1
for epoch in range(n_epochs):
    # update step sizes, learning rates
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())    
    
    # train on the training set
    losses_dist, losses_dist_vessel = [], []
    for fundus_batch, vessel_batch, coords_batch, filenames in train_batch_fetcher():
        # fundus_batch : (batch_size, 640, 640, 3)
        # vessel_batch : (batch_size, 640, 640, 1)
        # coords_batch : (batch_size, 2) (fv_y,fv_x)
        total_loss, loss_dist, loss_dist_vessel = network.train_on_batch([fundus_batch, vessel_batch], [coords_batch, coords_batch])
        losses_dist += [loss_dist] * len(filenames)
        losses_dist_vessel += [loss_dist_vessel] * len(filenames)
    print "loss_dist: {}, loss_dist_vessel: {},".format(np.mean(losses_dist), np.mean(losses_dist_vessel))
    
    # evaluate on validation set
    if epoch in validation_epochs:
        fundus_list, coords_list, predicted_coords_list, predicted_coords_vessel_list = [], [], [], []
        for fundus_batch, vessel_batch, coords_batch, filenames in validation_batch_fetcher():
            predicted_coords, predicted_coords_vessel = network.predict([fundus_batch, vessel_batch], batch_size=batch_size, verbose=0)
            coords_list.append(coords_batch)
            predicted_coords_list.append(predicted_coords)
            predicted_coords_vessel_list.append(predicted_coords_vessel)
            fundus_list.append(fundus_batch)
        val_predicted_coords = np.concatenate(predicted_coords_list, axis=0)
        val_coords = np.concatenate(coords_list, axis=0)
        val_fundus = np.concatenate(fundus_list, axis=0)
        Euclidean_fovea = utils.mean_Euclidean_fovea(val_coords, val_predicted_coords)
        utils.print_metrics(epoch + 1, Euclidean_fovea=Euclidean_fovea)
        
        # save the weight and validation results for current best
        if Euclidean_fovea < Best_Euclidean:
            network.save_weights(os.path.join(model_out_dir, "network_best_euclidean.h5"))
            Best_Euclidean = Euclidean_fovea
            utils.save_imgs_with_pts_fovea(val_fundus, val_predicted_coords, img_out_dir, epoch)

    sys.stdout.flush()
