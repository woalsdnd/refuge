from keras.models import model_from_json
import os
import utils
import numpy as np
import argparse
import pandas as pd

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
#     default="/home/woalsdnd/data/localization/ori_imgs",
    default="/home/woalsdnd/data/Offline_validation/",
    required=False
    )
parser.add_argument(
    '--img_anal_dir',
    type=str,
#     default='/home/woalsdnd/outputs/fovea_localization/Online_validation/img_result',
    default='/home/woalsdnd/outputs/fovea_localization/Offline_validation/img_result',
    required=False
    )
parser.add_argument(
    '--out_dir',
    type=str,
#     default="/home/woalsdnd/outputs/fovea_localization/Online_validation",
    default="/home/woalsdnd/outputs/fovea_localization/Offline_validation",
    required=False 
    )
parser.add_argument(
    '--localization_model_dir',
    type=str,
    default="/home/woalsdnd/models/fovea_limit_vessel_loss",
    required=False
    )

parser.add_argument(
    '--vessel_model_dir',
    type=str,
    default="/home/woalsdnd/models/vessel",
    required=False
)

FLAGS, _ = parser.parse_known_args()

# patch_size for od in original image
target_size = (640, 640)

# set gpu
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load model
localization_network = utils.load_network(FLAGS.localization_model_dir)
vessel_network = utils.load_network(FLAGS.vessel_model_dir)

# check save directory of img result
utils.mkdir_if_not_exist(FLAGS.img_anal_dir)

# iterate all image
filepaths = utils.all_files_under(FLAGS.img_dir)
submit_data = {"FileName":[], "X-coordinate":[], "Y-coordinate":[]}
for i, filepath in enumerate(filepaths):
    fn = os.path.basename(filepath)
    print "processing {}...".format(fn)
    
    # load an img (tensor shape of [1,h,w,3])
    img = utils.imagefiles2arrs([filepath])
    _, h, w, _ = img.shape
    resized_img = utils.resize_img(img, target_size)
        
    # run vessel inference
    vessel = vessel_network.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
    predicted_coords, _ = localization_network.predict([utils.normalize(resized_img, "od_from_fundus_vessel"),
                                                        (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
    
    # convert to coordinates in the original image
    coords = predicted_coords
    coords[0, 0] *= h 
    coords[0, 1] *= w 
    
    # save the result & img
    submit_data["FileName"].append(os.path.basename(filepath))
    submit_data["Y-coordinate"].append(coords[0, 0])
    submit_data["X-coordinate"].append(coords[0, 1])
    utils.save_imgs_with_pts(img.astype(np.uint8),
                                   coords.astype(np.uint16),
                                   FLAGS.img_anal_dir,
                                   os.path.basename(filepath).replace(".png", ""))
    
df = pd.DataFrame(data=submit_data)
utils.mkdir_if_not_exist(FLAGS.out_dir)
df[["FileName", "X-coordinate", "Y-coordinate"]].to_csv(os.path.join(FLAGS.out_dir, "submit.csv"), index=False)