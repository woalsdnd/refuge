import numpy as np
import pandas as pd
import argparse
import os

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gt_fovea',
    type=str,
    required=False
    )
parser.add_argument(
    '--pred_fovea',
    type=str,
    required=False
    )
FLAGS, _ = parser.parse_known_args()

gt_fovea = pd.read_csv(FLAGS.gt_fovea, delimiter=";")
pred_fovea = pd.read_csv(FLAGS.pred_fovea)
pred_fovea["id"] = pred_fovea["id"].map(lambda x: os.path.basename(x))

gt_fovea_image_no = gt_fovea.loc[:, "ImgName"].tolist()
pred_fovea_image_no = pred_fovea.loc[:, "id"].tolist()
gt_fovea_pos = np.array(zip(gt_fovea.loc[:, "Fovea_Y"].tolist(), gt_fovea.loc[:, "Fovea_X"].tolist()))
pred_fovea_pos = np.array(zip(pred_fovea.loc[:, "Y-coordinate"].tolist(), pred_fovea.loc[:, "X-coordinate"].tolist()))

assert (np.array(gt_fovea_image_no) == np.array(pred_fovea_image_no)).all()

fovea_dist = np.linalg.norm(gt_fovea_pos - pred_fovea_pos, axis=1)

print "fovea mean Euclidean dist: {}".format(np.mean(fovea_dist))

for rank in range(5):
    print "{}th worst fovea: {}".format(rank, gt_fovea_image_no[np.argsort(fovea_dist)[-1 * rank - 1]])
