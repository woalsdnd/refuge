import utils
import os
import numpy as np
from sklearn.metrics.classification import confusion_matrix
import argparse
from skimage import measure


def arrange_fns(all_filenames, dirname):
    val_filenames = [os.path.join(dirname, fn) for fn in ["g0006", "n0009", "n0049", "n0125", "n0159",
                                                        "n0298", "n0003", "n0040", "n0110", "n0144",
                                                        "n0271", "n0354"]]
    train_filenames = all_filenames[(all_filenames != os.path.join(dirname, "g0006.bmp")) & (all_filenames != os.path.join(dirname, "n0009.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0049.bmp")) & (all_filenames != os.path.join(dirname, "n0125.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0159.bmp")) & (all_filenames != os.path.join(dirname, "n0298.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0003.bmp")) & (all_filenames != os.path.join(dirname, "n0040.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0110.bmp")) & (all_filenames != os.path.join(dirname, "n0144.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0271.bmp")) & (all_filenames != os.path.join(dirname, "n0354.bmp")) & 
                 (all_filenames != os.path.join(dirname, "g0006.png")) & (all_filenames != os.path.join(dirname, "n0009.png")) & 
                 (all_filenames != os.path.join(dirname, "n0049.png")) & (all_filenames != os.path.join(dirname, "n0125.png")) & 
                 (all_filenames != os.path.join(dirname, "n0159.png")) & (all_filenames != os.path.join(dirname, "n0298.png")) & 
                 (all_filenames != os.path.join(dirname, "n0003.png")) & (all_filenames != os.path.join(dirname, "n0040.png")) & 
                 (all_filenames != os.path.join(dirname, "n0110.png")) & (all_filenames != os.path.join(dirname, "n0144.png")) & 
                 (all_filenames != os.path.join(dirname, "n0271.png")) & (all_filenames != os.path.join(dirname, "n0354.png")) ]
    train_filenames = [os.path.join(dirname, os.path.basename(fn).split(".")[0]) for fn in train_filenames]
    arranged_filenames = list(val_filenames) + list(train_filenames)
      
    return arranged_filenames
        

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--disc',
    type=bool,
    default=False
    )
parser.add_argument(
    '--cup',
    type=bool,
    default=False
    )
FLAGS, _ = parser.parse_known_args()

gt_dirs = []
pred_dirs = []
if FLAGS.disc:
    gt_dirs.append("../../data/preprocessed/od_segmentation/all_masks")
    pred_dirs.append("../../outputs/od_segmentation_reduced/submit")
if FLAGS.cup:
    gt_dirs.append("../../data/preprocessed/cup_segmentation/all_masks")
    pred_dirs.append("../../outputs/cup_segmentation_reduced/submit")

for i in range(len(gt_dirs)):
    gt_dir, pred_dir = gt_dirs[i], pred_dirs[i]
    gt_filenames = arrange_fns(np.array(utils.all_files_under(gt_dir)), gt_dir)
    pred_filenames = arrange_fns(np.array(utils.all_files_under(pred_dir)), pred_dir)
    list_sen, list_spe, list_jaccard, list_dice, list_dice_ori = [], [], [], [], []
    print "-"*20
    print "-"*20
    for index in range(len(gt_filenames)):
        # get gt and pred segs
        gt = utils.imagefiles2arrs([gt_filenames[index] + ".png"]).astype(np.uint8)[0, ...]
        if np.max(gt) == 255:
            gt[gt > 0] = 1
        pred = utils.imagefiles2arrs([pred_filenames[index] + ".bmp"]).astype(np.uint8)[0, ...]
        assert os.path.basename(gt_filenames[index]) == os.path.basename(pred_filenames[index])
        assert len(gt.shape) == 2 and len(pred.shape) == 2 
#         assert np.max(gt) == 1 and np.max(pred) == 1
        
        # compute sensitivity and specificity
        spe, sen, dice_val, jaccard_val = utils.seg_metrics(gt, pred)
        
        # print results and store to lists
        print "--- {} ---".format(os.path.basename(gt_filenames[index]))
        print "specificity: {}".format(spe)
        print "sensitivity: {}".format(sen)
        print "jaccard: {}".format(jaccard_val)
        print "dice: {}".format(dice_val)
        list_spe.append(spe)
        list_sen.append(sen)
        list_jaccard.append(jaccard_val)
        list_dice.append(dice_val)
        list_dice_ori.append(dice_val)
    
    # print all results & filenames
    print "mean_sen: {}, min_sen: {}, max_sen: {}".format(np.mean(list_sen), np.min(list_sen), np.max(list_sen))
    print "min_sen_file: {}, max_sen_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_sen)]), os.path.basename(gt_filenames[np.argmax(list_sen)]))
    print "mean_spe: {}, min_spe: {}, max_spe: {}".format(np.mean(list_spe), np.min(list_spe), np.max(list_spe))
    print "min_spe_file: {}, max_spe_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_spe)]), os.path.basename(gt_filenames[np.argmax(list_spe)]))
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard), np.min(list_jaccard), np.max(list_jaccard))
    print "min_jaccard_file: {}, max_jaccard_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_jaccard)]), os.path.basename(gt_filenames[np.argmax(list_jaccard)]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice), np.min(list_dice), np.max(list_dice))
    print "min_dice_file: {}, max_dice_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_dice)]), os.path.basename(gt_filenames[np.argmax(list_dice)]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori), np.min(list_dice_ori), np.max(list_dice_ori))
    print "min_dice_ori_file: {}, max_f1_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_dice_ori)]), os.path.basename(gt_filenames[np.argmax(list_dice_ori)]))
    
    print "results for validation"
    print "mean_sen: {}, min_sen: {}, max_sen: {}".format(np.mean(list_sen[:12]), np.min(list_sen[:12]), np.max(list_sen[:12]))
    print "mean_spe: {}, min_spe: {}, max_spe: {}".format(np.mean(list_spe[:12]), np.min(list_spe[:12]), np.max(list_spe[:12]))
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard[:12]), np.min(list_jaccard[:12]), np.max(list_jaccard[:12]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice[:12]), np.min(list_dice[:12]), np.max(list_dice[:12]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori[:12]), np.min(list_dice_ori[:12]), np.max(list_dice_ori[:12]))
    
    print "results for training"
    print "mean_sen: {}, min_sen: {}, max_sen: {}".format(np.mean(list_sen[12:]), np.min(list_sen[12:]), np.max(list_sen[12:]))
    print "mean_spe: {}, min_spe: {}, max_spe: {}".format(np.mean(list_spe[12:]), np.min(list_spe[12:]), np.max(list_spe[12:]))
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard[12:]), np.min(list_jaccard[12:]), np.max(list_jaccard[12:]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice[12:]), np.min(list_dice[12:]), np.max(list_dice[12:]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori[12:]), np.min(list_dice_ori[12:]), np.max(list_dice_ori[12:]))
    
