import os

from keras import backend as K
from keras import objectives
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dense
from keras.layers import Input
from keras.layers.core import Activation, Flatten
from keras.layers.merge import Concatenate
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import Adam
from keras.layers.core import Lambda
import numpy as np
from keras.optimizers import SGD

os.environ['KERAS_BACKEND'] = 'tensorflow'
K.set_image_dim_ordering('tf')


def dice(y_true, y_pred):
    intersection = K.sum(y_true * y_pred, axis=[1, 2, 3])
    union = K.sum(y_true, axis=[1, 2, 3]) + K.sum(y_pred, axis=[1, 2, 3])
    return K.mean((2. * intersection + K.epsilon()) / (union + K.epsilon()), axis=0)


def conv_blocks(n_block, x, n_filters, filter_size, padding='same', strides=(1, 1), dilation_rate=(1, 1)):
    conv = x
    for _ in range(n_block):
        conv = Conv2D(n_filters, filter_size, strides=strides, padding=padding, dilation_rate=dilation_rate)(conv)
#         conv = BatchNormalization(scale=False, axis=3)(conv)
        conv = Activation('relu')(conv) 
    return conv


def seg_from_fundus_vessel(img_size, n_maxpool, labmda_val, vessel_fundus_filter_ratio):
        
    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    img_ch_vessel = 1  # image channels
    out_ch_vessel = 1  # output channel
    img_ch_fundus = 3  # image channels
    out_ch_fundus_vessel = 1  # output channel
    img_height, img_width = img_size[0], img_size[1]
    n_init_filters_vessel = 16
    n_init_filters_fundus = 32
    max_depth_vessel = 256
    max_depth_fundus = 512
    n_filters_last_vessel = min(n_init_filters_vessel * (2 ** n_maxpool), max_depth_vessel)
    n_filters_last_fundus = min(n_init_filters_fundus * (2 ** n_maxpool), max_depth_fundus)
    padding = 'same'
    
    # vessel branch
    input_vessel = Input((img_height, img_width, img_ch_vessel))
    list_out = [input_vessel]
    # encode with maxpooling
    for index in range(n_maxpool):
        conv = conv_blocks(2, list_out[-1], min(n_init_filters_vessel * (2 ** index), max_depth_vessel), (k, k), padding=padding)
        pool = MaxPooling2D(pool_size=(s, s))(conv)
        list_out.append(pool)
    conv = conv_blocks(2, list_out[-1], n_filters_last_vessel, (k, k), padding=padding)
    list_out.append(conv)
    # n_maxpool : 5 4 3 2
    # n_dilation: 3 4 5 6
    n_dilation = 8 - n_maxpool
    n_dilated_filters = int(vessel_fundus_filter_ratio * n_filters_last_fundus // (n_dilation))
    
    list_dilated_conv = []
    for index in range(n_dilation):
        list_dilated_conv.append(Conv2D(n_dilated_filters, (k, k), dilation_rate=(2 ** index, 2 ** index), padding=padding)(list_out[-1]))
    vessel_block = Concatenate(axis=3)(list_dilated_conv)  
    output_vessel = Conv2D(out_ch_vessel, (1, 1), padding=padding, activation='sigmoid')(vessel_block)
    
    input_fundus = Input((img_height, img_width, img_ch_fundus))
    list_out = [input_fundus]
    list_conv = []
    # encode with maxpooling
    for index in range(n_maxpool):
        conv = conv_blocks(2, list_out[-1], min(n_init_filters_fundus * (2 ** index), max_depth_fundus), (k, k), padding=padding)
        pool = MaxPooling2D(pool_size=(s, s))(conv)
        list_out.append(pool)
        list_conv.append(conv)
    conv = conv_blocks(2, list_out[-1], n_filters_last_fundus, (k, k), padding=padding)
    concated_block = Concatenate(axis=3)([conv, vessel_block])
    list_out.append(concated_block)
    
    # decode with upsample
    for index in range(n_maxpool, 0, -1):
        up = UpSampling2D(size=(s, s))(list_out[-1])
        concat = Concatenate(axis=3)([up, list_conv[index - 1]])
        conv = conv_blocks(2, concat, min(n_init_filters_fundus * (2 ** (index - 1)), max_depth_fundus), (k, k), padding=padding)
        list_out.append(conv)
    
    # sigmoid 
    output_fundus_vessel = Conv2D(out_ch_fundus_vessel, (1, 1), padding=padding, activation='sigmoid')(list_out[-1])
    
    model = Model([input_fundus, input_vessel], [output_fundus_vessel, output_vessel])
    
    def seg_loss(y_true, y_pred):
        y_true_flat = K.batch_flatten(y_true)
        y_pred_flat = K.batch_flatten(y_pred)
        return objectives.binary_crossentropy(y_true_flat, y_pred_flat)
    
    model.compile(optimizer=Adam(lr=0, beta_1=0.5), loss=[seg_loss, seg_loss], loss_weights=[1, labmda_val], metrics=[dice])

    return model

