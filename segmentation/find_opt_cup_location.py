from skimage import exposure
from skimage import io, exposure, img_as_uint, img_as_float
from scipy.misc import imread, imresize, imsave
from PIL import Image

import pandas as pd
import numpy as np

import argparse
import cv2
import os

import utils


def load_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--vessel_path', 
                        required=False, 
                        default="/home/woalsdnd/outputs/vessels_segmentation/training_only_given_data",
                        type=str,
                        help='fundas annotation img path')
    
    parser.add_argument('--disc_path',
                        required=False,
                        default='/home/woalsdnd/data/segmentation/Training400/disc',
                        type=str,
                        help='disc path')
    
    parser.add_argument('--cup_path',
                        default='/home/woalsdnd/data/segmentation/Training400/cup',
                        type=str,
                        help='cup path')
    
    parser.add_argument('--gpu_index',
                        required=True,
                        default="0",
                        type=str,
                        help='gpu index')
    
    parser.add_argument('--save_disc_path',
                        default='/home/kimjeyoung/competition/check_cup/disc',
                        type=str,
                        help='check disc location')
    
    parser.add_argument('--offset_iter',
                        default=20,
                        type=str,
                        help='apply multiple iter')
    
    parser.add_argument('--rimone_path',
                        default='/home/woalsdnd/data/segmentation/rimone/disc',
                        type=str,
                        help='rimone dataset path')
    
    return parser.parse_args()


def get_disc_annotation(path, save_path, iteration, mean_h, mean_w):
    
    # write original image with added contours to disk  
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    
    disc = []
    
    # set path
    mask_full_path = os.path.join(path, "masks")
    mask_file_lists = sorted(os.listdir(mask_full_path))[:10]
    
    origin_full_path = os.path.join(path, "imgs")
    origin_file_lists = sorted(os.listdir(origin_full_path))[:10]
    
    for i in range(2, iteration):
        print("Iteration {}".format(i))
        # mkdir
        img_save_dir = os.path.join(save_path, str(i))
        channel_save_dir = os.path.join(img_save_dir, "channel_wise")
        
        if not os.path.exists(img_save_dir):
            os.mkdir(img_save_dir)
            os.makedirs(channel_save_dir)

        for mask_img_name, origin_img_name in zip(mask_file_lists, origin_file_lists) :

            # load annotation image and original
            mask_img = imread(os.path.join(mask_full_path, mask_img_name))
            origin_img = imread(os.path.join(origin_full_path, origin_img_name))

            # tresholding
            ret, thresh = cv2.threshold(mask_img, 127, 255,0)
            mask_img, contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            cnt = contours[0]
            x,y,w,h = cv2.boundingRect(cnt)

            # crop image
            y_1 = y-i*OFFSET
            if y_1 < 0:
                y_1 =0
            x_1 = x-i*OFFSET
            if x_1 < 0:
                x_1 = 0

            crop_img = origin_img[y_1:y+h+i*OFFSET, x_1:x+w+i*OFFSET]
            
            ex_crop_img = np.expand_dims(crop_img, axis=0)

            # image normalization
            norm_image = utils.normalize(ex_crop_img, task="od_from_fundus_vessel")
            norm_image = np.squeeze(norm_image, axis=0)
            
            # channel wise
            r_channel_img = norm_image[:, :, 0]
            g_channel_img = norm_image[:, :, 1]
            b_channel_img = norm_image[:, :, 2]
            
            r_channel_img = 2*r_channel_img
            r_channel_img = r_channel_img
            print(r_channel_img)
            print(np.max(r_channel_img))
            print(np.min(r_channel_img))
            print("-------------------")
            
            g_channel_img = 3*g_channel_img+2
            g_channel_img = g_channel_img
            print(np.max(g_channel_img))
            print(np.min(g_channel_img))
            print("-------------------")
            
            
            b_channel_img = 2*b_channel_img+3
            b_channel_img = b_channel_img
            print(np.max(b_channel_img))
            print(np.min(b_channel_img))
            print("-------------------")
            
            trans_img = np.stack([r_channel_img, g_channel_img, b_channel_img], axis=-1)
            
            
            # channel-wise image save
            imsave(os.path.join(channel_save_dir, 
                                "{}_r.jpg".format(origin_img_name[:-4])), r_channel_img)
            imsave(os.path.join(channel_save_dir, 
                                "{}_g.jpg".format(origin_img_name[:-4])), g_channel_img)
            imsave(os.path.join(channel_save_dir, 
                                "{}_b.jpg".format(origin_img_name[:-4])), b_channel_img)
            
            imsave(os.path.join(channel_save_dir, 
                                "{}_t.jpg".format(origin_img_name[:-4])), trans_img)

            # images result save
            imsave(os.path.join(img_save_dir, "{}_norm.jpg".format(origin_img_name[:-4])), norm_image)
            imsave(os.path.join(img_save_dir, "{}_origin.jpg".format(origin_img_name[:-4])), crop_img)


def check_rimone_statistic(path):
    rimone_images = []
    img_path = os.path.join(path, "imgs")
    mask_path = os.path.join(path, "masks")
    
    img_file_lists = sorted(os.listdir(img_path))
    mask_file_lists = sorted(os.listdir(mask_path))
    
    num_data = len(img_file_lists)
    img_w = []
    img_h = []
    for i in range(num_data):
        img = imread(os.path.join(img_path, img_file_lists[i]))
        
        assert img.shape[-1] == 3
        
        img_h.append(img.shape[0])
        img_w.append(img.shape[1])
        
        
    mean_h = int(np.mean(img_h))
    mean_w = int(np.mean(img_w))
    print("Rimone dataset mean w : {} , mean h :{}".format(mean_h, mean_w))
    return mean_h, mean_w 

if __name__ == "__main__":
    
    OFFSET = 40
    
    # add parser
    args = load_parse()
    
    #GPU
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu_index
    
    # check     
    mean_h, mean_w = check_rimone_statistic(args.rimone_path)
    
    # get disc image
    get_disc_annotation(args.disc_path, args.save_disc_path, args.offset_iter,
                       mean_h, mean_w)
        
        