from skimage import exposure
from skimage import io, exposure, img_as_uint, img_as_float
from scipy.misc import imread, imresize, imsave
from PIL import Image

import pandas as pd
import numpy as np

import argparse
import cv2
import os

import utils

def load_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--vessel_path', 
                        required=False, 
                        default="/home/woalsdnd/outputs/vessels_segmentation/training_only_given_data",
                        type=str,
                        help='fundas annotation img path')
    
    parser.add_argument('--train_path',
                        default='/home/woalsdnd/data/segmentation/Training400',
                        type=str,
                        help='cup path')
    
    parser.add_argument('--cup_out',
                        default="../data/preprocessed/cup_segmentation_around_od",
                        type=str,
                        help='check disc location')
    
    parser.add_argument('--disc_out',
                        default="../data/preprocessed/od_segmentation_around_od",
                        type=str,
                        help='apply multiple iter')
    
    parser.add_argument('--rimone_path',
                        default='/home/woalsdnd/data/segmentation/rimone',
                        type=str,
                        help='rimone dataset path')
    
    parser.add_argument('--riga_path',
                        default='/home/woalsdnd/data/segmentation/riga',
                        type=str,
                        help='rimone dataset path')

    
    return parser.parse_args()


def make_output_dir(disc_path, cup_path):
    if not os.path.exists(disc_path):
        for path in [disc_path, cup_path]:
            for name in ["train", "validation"]:
                os.makedirs(os.path.join(path, "{}/imgs".format(name)))
                os.makedirs(os.path.join(path, "{}/masks".format(name)))
                
def load_image_data(riga_dir, rimone_dir, train_dir, disc_out, cup_out):
    
    for i, path in enumerate([rimone_dir, train_dir]):

        # setting directory
        disc_dir = os.path.join(path, "disc")
        cup_dir = os.path.join(path, "cup")

        disc_mask_dir = os.path.join(disc_dir, "masks")
        disc_img_dir = os.path.join(disc_dir, "imgs")

        cup_mask_dir = os.path.join(cup_dir, 'masks')
        cup_img_dir = os.path.join(cup_dir, 'imgs')

        categories = [[disc_mask_dir, disc_img_dir], [cup_mask_dir, cup_img_dir]]

        # iterate disc , cup
        for j, category in enumerate(categories):
            mask_lists = sorted(os.listdir(category[0]))
            ori_img_lists = sorted(os.listdir(category[1]))

            for ori_img_list, mask_list in zip(ori_img_lists, mask_lists):
                mask_img = imread(os.path.join(category[0], mask_list))
                origin_img = imread(os.path.join(category[1], ori_img_list))

                # assert 
                assert mask_img.shape[0] == origin_img.shape[0]

                # tresholding
                ret, thresh = cv2.threshold(mask_img, 127, 255,0)
                mask_img, contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

                try:
                    cnt = contours[0]
                except:
                    print("error : {}".format(mask_list))
                x,y,w,h = cv2.boundingRect(cnt)

                # center point
                center_y = int((2*y+h)/2)
                center_x = int((2*x+w)/2)

                x_1 = center_x - OFFSET
                y_1 = center_y - OFFSET
                
                pad_w = abs(center_x - OFFSET)
                pad_h = abs(center_y - OFFSET)

                x_2 = center_x + OFFSET
                y_2 = center_y + OFFSET
                
                pad_w2 = mask_img.shape[1]-x_2
                pad_h2 = mask_img.shape[0]-y_2
                
                if x_2 > mask_img.shape[1]:
                    x_2 = mask_img.shape[1]
                if y_2 > mask_img.shape[0]:
                    y_2 = mask_img.shape[0]
                if x_1 < 0:
                    x_1 = 0
                if y_1 < 0:
                    y_1 = 0


                # crop image and mask

                crop_img = origin_img[y_1:y_2, x_1:x_2]
                crop_mask = mask_img[y_1:y_2, x_1:x_2]
                
                print(origin_img.shape, crop_img.shape, pad_w, pad_h, x_2, x_1, y_2, y_1, center_x, center_y)
                
                if crop_img.shape[0] == 512 and crop_img.shape[1] == 512:
                    image = crop_img
                    mask = crop_mask
                else:

                    if x_1 == 0 and y_1 == 0 and x_2 != mask_img.shape[1] and y_2 != mask_img.shape[0]:

                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512, 512])

                        pad_img[pad_h:, pad_w:] = crop_img
                        pad_mask[pad_h:, pad_w:] = crop_mask

                        image = pad_img
                        mask = pad_mask
                    elif x_1 == 0 and y_1 != 0 and x_2 != mask_img.shape[1] and y_2 != mask_img.shape[0]:

                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512, 512])

                        pad_img[:, pad_w:] = crop_img
                        pad_mask[:, pad_w:] = crop_mask

                        image = pad_img
                        mask = pad_mask

                    elif x_1 != 0 and y_1 == 0 and x_2 != mask_img.shape[1] and y_2 != mask_img.shape[0]:

                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512, 512])

                        pad_img[pad_h:, :] = crop_img
                        pad_mask[pad_h:, :] = crop_mask

                        image = pad_img
                        mask = pad_mask


                    elif x_2 == mask_img.shape[1] and y_2 == mask_img.shape[0] and x_1 != 0 and y_1 != 0:

                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512,512])

                        pad_img[:pad_h2, :pad_w2] = crop_img
                        pad_mask[:pad_h2, :pad_w2] = crop_mask

                        image = pad_img
                        mask = pad_mask

                    elif x_2 != mask_img.shape[1] and y_2 == mask_img.shape[0] and x_1 != 0 and y_1 != 0:

                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512,512])

                        pad_img[:pad_h2, :] = crop_img
                        pad_mask[:pad_h2, :] = crop_mask

                        image = pad_img
                        mask = pad_mask

                    elif x_2 == mask_img.shape[1] and y_2 != mask_img.shape[0] and x_1 != 0 and y_1 != 0:
                        
                        pad_img = np.zeros([512, 512, 3])
                        pad_mask = np.zeros([512,512])

                        pad_img[:, :pad_w2] = crop_img
                        pad_mask[:, :pad_w2] = crop_mask

                        image = pad_img
                        mask = pad_mask

                    
                
                
                assert image.shape[0] == 512 and image.shape[1] == 512

                if i == 0:
                    dataset = 'train'
                if i == 1:
                    dataset = 'validation'
                if j == 0:
                    output = disc_out
                if j == 1:
                    output = cup_out

                imsave(output+"/{}/{}/{}".format(dataset, "imgs", ori_img_list), image)
                imsave(output+"/{}/{}/{}".format(dataset, "masks", mask_list), mask)

                
                

if __name__ == "__main__":
    
    OFFSET = 256
    IMGSIZE = 512
    
    args = load_parse()
    make_output_dir(args.disc_out, args.cup_out)
    
    total_image = load_image_data(args.riga_path, args.rimone_path, args.train_path, args.disc_out, args.cup_out)
    
    
    