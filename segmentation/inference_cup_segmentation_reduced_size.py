from PIL import Image
from keras.models import model_from_json
import os
import utils
import numpy as np
import argparse

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    default="/home/woalsdnd/data/segmentation/Training400/cup/imgs",
    required=False
    )
parser.add_argument(
    '--img_anal_dir',
    type=str,
    default="/home/woalsdnd/outputs/cup_segmentation_reduced",
    required=False
    )
parser.add_argument(
    '--seg_model_dir',
    type=str,
    default='/home/woalsdnd/models/cup_segmentation',
    required=False
)
parser.add_argument(
    '--vessel_model_dir',
    type=str,
    default='/home/woalsdnd/models/vessel',
    required=False
)

FLAGS, _ = parser.parse_known_args()

# misc params
target_size = (640, 640)
intensity_threshold = 0.5

# set gpu
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load the models and corresponding weights
seg_model = utils.load_network(FLAGS.seg_model_dir)
vessel_model = utils.load_network(FLAGS.vessel_model_dir)

# make directories
out_vessel_dir_visualzation = os.path.join(FLAGS.img_anal_dir, "vessel_visualization")
out_cup_dir_visualization = os.path.join(FLAGS.img_anal_dir, "submit_visualization")
out_cup_dir = os.path.join(FLAGS.img_anal_dir, "submit")
utils.mkdir_if_not_exist(out_vessel_dir_visualzation)
utils.mkdir_if_not_exist(out_cup_dir_visualization)
utils.mkdir_if_not_exist(out_cup_dir)

# iterate all images
filenames = utils.all_files_under(FLAGS.img_dir)
for filename in filenames:
    print "processing {}...".format(os.path.basename(filename))
    
    # load an img (tensor shape of [1,h,w,3])
    img = utils.imagefiles2arrs([filename])
    _, h, w, _ = img.shape
    resized_img = utils.resize_img(img, target_size)
    
    # run inference
    vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
    cup_seg, _ = seg_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"),
                                                     (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
    
    # upscale & cut by threshold
    cup_seg = cup_seg[0, ..., 0]
    cup_seg[cup_seg < intensity_threshold] = 0
    cup_seg[cup_seg >= intensity_threshold] = 1
    
    # fill-in image
    if len(cup_seg[cup_seg == 1]) != 0:
        result = utils.fill_in_img(cup_seg)
        result = utils.remain_largest_blob(result)
    else:
        result = cup_seg
    
    # save the result
    Image.fromarray(result.astype(np.uint8)).save(os.path.join(out_cup_dir, os.path.basename(filename).replace(".jpg", ".bmp")))
    Image.fromarray((vessel[0, ..., 0] * 255).astype(np.uint8)).save(os.path.join(out_vessel_dir_visualzation, os.path.basename(filename)))
    Image.fromarray((result * 255).astype(np.uint8)).save(os.path.join(out_cup_dir_visualization, os.path.basename(filename).replace(".jpg", ".bmp")))

