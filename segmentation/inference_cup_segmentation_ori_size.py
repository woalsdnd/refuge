from PIL import Image
from keras.models import model_from_json
import os
import utils
import numpy as np
import argparse
from keras import backend as K

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--dataset_type',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# misc params
target_size = (640, 640)
intensity_threshold = 0.5

# set gpu
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load the models and corresponding weights
seg_model_dir = '/home/woalsdnd/models/cup_segmentation'
vessel_model_dir = '/home/woalsdnd/models/vessel'
seg_model = utils.load_network(seg_model_dir)
vessel_model = utils.load_network(vessel_model_dir)

# make directories
img_anal_dir = "/home/woalsdnd/outputs/cup_segmentation"
if FLAGS.dataset_type == "training":
    img_dir = "/home/woalsdnd/data/segmentation/Training400/cup/imgs"
    out_vessel_dir_visualzation = os.path.join(img_anal_dir, "vessel_train_visualization")
    out_cup_dir_visualization = os.path.join(img_anal_dir, "submit_train_visualization")
    out_cup_dir = os.path.join(img_anal_dir, "submit_train")
elif FLAGS.dataset_type == "offline_validation":
    img_dir = "/home/woalsdnd/data/Offline_validation"
    out_vessel_dir_visualzation = os.path.join(img_anal_dir, "vessel_offline_validation_visualization")
    out_cup_dir_visualization = os.path.join(img_anal_dir, "submit_offline_validation_visualization")
    out_cup_dir = os.path.join(img_anal_dir, "submit_offline_validation")
elif FLAGS.dataset_type == "online_validation":
    img_dir = "/home/woalsdnd/data/Online_validation"
    out_vessel_dir_visualzation = os.path.join(img_anal_dir, "vessel_online_validation_visualization")
    out_cup_dir_visualization = os.path.join(img_anal_dir, "submit_online_validation_visualization")
    out_cup_dir = os.path.join(img_anal_dir, "submit_online_validation")
utils.mkdir_if_not_exist(out_vessel_dir_visualzation)
utils.mkdir_if_not_exist(out_cup_dir_visualization)
utils.mkdir_if_not_exist(out_cup_dir)

# iterate all images
filenames = utils.all_files_under(img_dir)
for filename in filenames:
    print "processing {}...".format(os.path.basename(filename))

    # load an img (tensor shape of [1,h,w,3])
    img = utils.imagefiles2arrs([filename])
    _, h, w, _ = img.shape
    resized_img = utils.resize_img(img, target_size)
    
    # run inference
    vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
    cup_seg, _ = seg_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"),
                                                     (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
    
    # upscale & cut by threshold
    cup_seg = cup_seg[0, ..., 0]
    cup_seg_in_ori_scale = utils.resize_img(cup_seg, (h, w))
    cup_seg_in_ori_scale[cup_seg_in_ori_scale < intensity_threshold] = 0
    cup_seg_in_ori_scale[cup_seg_in_ori_scale >= intensity_threshold] = 1
      
    # fill-in image
    result = cup_seg_in_ori_scale
    result = utils.fill_in_img(cup_seg_in_ori_scale)
    result = utils.remain_largest_blob(result)
    result = utils.convex_hull(result)

    # save the result
    Image.fromarray((vessel[0, ..., 0] * 255).astype(np.uint8)).save(os.path.join(out_vessel_dir_visualzation, os.path.basename(filename)))
    Image.fromarray(result.astype(np.uint8)).save(os.path.join(out_cup_dir, os.path.basename(filename).replace(".jpg", ".bmp")))
    Image.fromarray((result * 255).astype(np.uint8)).save(os.path.join(out_cup_dir_visualization, os.path.basename(filename).replace(".jpg", ".bmp")))
