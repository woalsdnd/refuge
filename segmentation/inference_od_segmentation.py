from PIL import Image
from keras.models import model_from_json
import os
import utils
import numpy as np
import argparse

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    default="/home/woalsdnd/data/Offline_validation",
#     default="/home/woalsdnd/data/Online_validation",
    required=False
    )
parser.add_argument(
    '--img_anal_dir',
    type=str,
    default="/home/woalsdnd/outputs/od_segmentation",
    required=False
    )
parser.add_argument(
    '--seg_model_dir',
    type=str,
    default='/home/woalsdnd/models/disc_segmentation',
    required=False
)
parser.add_argument(
    '--vessel_model_dir',
    type=str,
    default='/home/woalsdnd/models/vessel',
    required=False
)

FLAGS, _ = parser.parse_known_args()

# misc params
target_size = (640, 640)
intensity_threshold = 0.5

# set gpu
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load the models and corresponding weights
seg_model = utils.load_network(FLAGS.seg_model_dir)
vessel_model = utils.load_network(FLAGS.vessel_model_dir)

# make directories
out_vessel_dir_visualzation = os.path.join(FLAGS.img_anal_dir, "vessel_offline_validation_visualization")
out_od_dir_visualization = os.path.join(FLAGS.img_anal_dir, "submit_offline_validation_visualization")
out_od_dir = os.path.join(FLAGS.img_anal_dir, "submit_offline_validation")
# out_vessel_dir_visualzation = os.path.join(FLAGS.img_anal_dir, "vessel_online_validation_visualization")
# out_od_dir_visualization = os.path.join(FLAGS.img_anal_dir, "submit_online_validation_visualization")
# out_od_dir = os.path.join(FLAGS.img_anal_dir, "submit_online_validation")
utils.mkdir_if_not_exist(out_vessel_dir_visualzation)
utils.mkdir_if_not_exist(out_od_dir_visualization)
utils.mkdir_if_not_exist(out_od_dir)

# iterate all images
filenames = utils.all_files_under(FLAGS.img_dir)
for filename in filenames:
    fn = os.path.basename(filename)
    print "processing {}...".format(fn)

    # load an img (tensor shape of [1,h,w,3])
    img = utils.imagefiles2arrs([filename])
    _, h, w, _ = img.shape
    resized_img = utils.resize_img(img, target_size)
    
    # run inference
    vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
    od_seg, _ = seg_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"),
                                                     (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
    
    # upscale & cut by threshold
    od_seg = od_seg[0, ..., 0]
    od_seg_in_ori_scale = utils.resize_img(od_seg, (h, w))
    od_seg_in_ori_scale[od_seg_in_ori_scale < intensity_threshold] = 0
    od_seg_in_ori_scale[od_seg_in_ori_scale >= intensity_threshold] = 1
    
    # fill-in image
    result = utils.fill_in_img(od_seg_in_ori_scale)
    result = utils.remain_largest_blob(result)
    
    # save the result
    Image.fromarray((vessel[0, ..., 0] * 255).astype(np.uint8)).save(os.path.join(out_vessel_dir_visualzation, os.path.basename(filename)))
    Image.fromarray(result.astype(np.uint8)).save(os.path.join(out_od_dir, os.path.basename(filename).replace(".jpg", ".bmp")))
    Image.fromarray((result * 255).astype(np.uint8)).save(os.path.join(out_od_dir_visualization, os.path.basename(filename).replace(".jpg", ".bmp")))

