import utils
import os
import numpy as np
from sklearn.metrics.classification import confusion_matrix
import argparse
from skimage import measure


def arrange_fns(all_filenames, dirname):
    val_filenames = [os.path.join(dirname, fn) for fn in ["g0006.bmp", "n0009.bmp", "n0049.bmp", "n0125.bmp", "n0159.bmp",
                                                        "n0298.bmp", "n0003.bmp", "n0040.bmp", "n0110.bmp", "n0144.bmp",
                                                        "n0271.bmp", "n0354.bmp"]]
    train_filenames = all_filenames[(all_filenames != os.path.join(dirname, "g0006.bmp")) & (all_filenames != os.path.join(dirname, "n0009.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0049.bmp")) & (all_filenames != os.path.join(dirname, "n0125.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0159.bmp")) & (all_filenames != os.path.join(dirname, "n0298.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0003.bmp")) & (all_filenames != os.path.join(dirname, "n0040.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0110.bmp")) & (all_filenames != os.path.join(dirname, "n0144.bmp")) & 
                 (all_filenames != os.path.join(dirname, "n0271.bmp")) & (all_filenames != os.path.join(dirname, "n0354.bmp")) ]
    arranged_filenames = list(val_filenames) + list(train_filenames)
    
    return arranged_filenames

        
def dice_jaccard(true, pred, val):
    TP = len(true[(true == val) & (pred == val)])
    TN = len(true[(true != val) & (pred != val)])
    FP = len(true[(true != val) & (pred == val)])
    FN = len(true[(true == val) & (pred != val)])
    dice_val = 2.*TP / (FN + 2 * TP + FP)
    jaccard_val = 1.*TP / (FN + TP + FP)
    return dice_val, jaccard_val


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_dir',
    type=str,
    default=False
    )
parser.add_argument(
    '--gt_dir',
    type=str,
    default=False
    )
FLAGS, _ = parser.parse_known_args()

gt_dir, pred_dir = FLAGS.gt_dir, FLAGS.pred_dir
gt_filenames = arrange_fns(np.array(utils.all_files_under(gt_dir)), gt_dir)
pred_filenames = arrange_fns(np.array(utils.all_files_under(pred_dir)), pred_dir)
for thr_val in [0, 128]:
    list_jaccard, list_dice, list_dice_ori = [], [], []
    print "-"*20
    print "-"*20
    for index in range(len(gt_filenames)):
        # get gt and pred segs
        gt = utils.imagefiles2arrs(gt_filenames[index:index + 1]).astype(np.uint8)[0, ...]
        pred = utils.imagefiles2arrs(pred_filenames[index:index + 1]).astype(np.uint8)[0, ...]
        assert os.path.basename(gt_filenames[index]) == os.path.basename(pred_filenames[index])
        assert len(gt.shape) == 2 and len(pred.shape) == 2 
    #     assert np.max(gt) == 1 and np.max(pred) == 1
        
        # compute sensitivity and specificity
        dice_val, jaccard_val = dice_jaccard(gt, pred, thr_val)
        
        # print results and store to lists
        print "--- {} ---".format(os.path.basename(gt_filenames[index]))
        print "jaccard: {}".format(jaccard_val)
        print "dice: {}".format(dice_val)
        list_jaccard.append(jaccard_val)
        list_dice.append(dice_val)
        list_dice_ori.append(dice_val)
    
    # print all results & filenames
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard), np.min(list_jaccard), np.max(list_jaccard))
    print "min_jaccard_file: {}, max_jaccard_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_jaccard)]), os.path.basename(gt_filenames[np.argmax(list_jaccard)]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice), np.min(list_dice), np.max(list_dice))
    print "min_dice_file: {}, max_dice_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_dice)]), os.path.basename(gt_filenames[np.argmax(list_dice)]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori), np.min(list_dice_ori), np.max(list_dice_ori))
    print "min_dice_ori_file: {}, max_f1_file: {}".format(os.path.basename(gt_filenames[np.argmin(list_dice_ori)]), os.path.basename(gt_filenames[np.argmax(list_dice_ori)]))
    
    print "results for validation"
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard[:12]), np.min(list_jaccard[:12]), np.max(list_jaccard[:12]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice[:12]), np.min(list_dice[:12]), np.max(list_dice[:12]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori[:12]), np.min(list_dice_ori[:12]), np.max(list_dice_ori[:12]))
    
    print "results for training"
    print "mean_jaccard: {}, min_jaccard: {}, max_jaccard: {}".format(np.mean(list_jaccard[12:]), np.min(list_jaccard[12:]), np.max(list_jaccard[12:]))
    print "mean_dice: {}, min_dice: {}, max_dice: {}".format(np.mean(list_dice[12:]), np.min(list_dice[12:]), np.max(list_dice[12:]))
    print "mean_dice_ori: {}, min_dice_ori: {}, max_dice_ori: {}".format(np.mean(list_dice_ori[12:]), np.min(list_dice_ori[12:]), np.max(list_dice_ori[12:]))
    
