import numpy as np
from PIL import Image
import os
from subprocess import Popen, PIPE


def all_files_under(path, extension=None, append_path=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    return filenames


# sort by # foreground (value > 127)
fns = all_files_under("./", extension=".png")
list_n_foreground = []
for fn in fns:
    arr = np.array(Image.open(fn))
    list_n_foreground.append((len(arr[arr > 127]), fn))
list_n_foreground.sort(key=lambda x:x[0], reverse=True)

# read output
with open("out_od_seg") as f:
    lines = f.readlines()
jaccard_dict = {}
for line in lines:
    if "*** Epoch" in line:  
        jaccard_dict[int(line[line.find("*** Epoch") + 10:line.find("  ====>")])] = float(line[line.find("jaccard_val : ") + 14:-4])

# print if jaccard is bigger than the threshold
jaccard_thresh = 0.89
remain = []
for n, path in list_n_foreground:
    epoch = int(path[path.find("_06_od") - 3:path.find("_06_od")])
    if jaccard_dict[epoch] > jaccard_thresh:
        remain.append(path)

# delete bottoms
for n, path in list_n_foreground:
    if not path in remain:
        pipes = Popen(["rm", path], stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()  

# delete bottoms
# selected_paths = list_n_foreground[50:]
# for n, selected_path in selected_paths:
#     pipes = Popen(["rm", selected_path, stdout=PIPE, stderr=PIPE)
#     std_out, std_err = pipes.communicate()  
