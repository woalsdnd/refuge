from PIL import Image
import numpy as np
import argparse
import utils
import os

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--cup_segmentation',
    type=str,
    required=True
    )
parser.add_argument(
    '--disc_segmentation',
    type=str,
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

cup_seg_fpaths = utils.all_files_under(FLAGS.cup_segmentation)
disc_seg_fpaths = utils.all_files_under(FLAGS.disc_segmentation)
utils.mkdir_if_not_exist(FLAGS.out_dir)

for index in range(len(cup_seg_fpaths)):
    cup_seg_fpath = cup_seg_fpaths[index]
    disc_seg_fpath = disc_seg_fpaths[index]
    assert os.path.basename(cup_seg_fpath) == os.path.basename(disc_seg_fpath)

    cup_seg = np.array(Image.open(cup_seg_fpath))
    disc_seg = np.array(Image.open(disc_seg_fpath))
    
    submit_arr = np.ones(cup_seg.shape)*255
    submit_arr[disc_seg == 1] = 128
    submit_arr[cup_seg == 1] = 0
    Image.fromarray(submit_arr.astype(np.uint8)).save(os.path.join(FLAGS.out_dir, os.path.basename(cup_seg_fpath)))
